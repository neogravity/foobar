import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Router } from '@angular/router';
import { NbToastrService } from '@nebular/theme';
import { map } from 'rxjs/operators';
import { Policie } from '../_models/Policie';
import { PaginatedResult } from '../_models/pagination';
import { RequestListCriteriaDto } from '../DtoView/BaseDto/RequestListCriteriaDto';
import { Observable } from 'rxjs';
import { PolicyFilterDto } from '../DtoView/create-claim/PolicyFilterDto';
import { checkUndefinedAndRemove } from '../_helpers/checkUndefinedAndRemove';

@Injectable({
  providedIn: 'root'
})
export class PolicyService {
  baseUrl = environment.apiUrl + 'policy/';
  lang: string = localStorage.getItem('lang');

  constructor(private http: HttpClient, private router: Router, private toastrService: NbToastrService) { }

  policySearch(page: number, search: RequestListCriteriaDto<PolicyFilterDto>) : Observable<PaginatedResult<Policie[]>> {
    const paginatedResult: PaginatedResult<Policie[]> = new PaginatedResult<Policie[]>();
    let params = new HttpParams();
    let itemsPerPage = "10";

    if (page != null && itemsPerPage != null) {
      params = params.append('Pagination.pageNumber', page.toString());
      params = params.append('Pagination.pageSize', itemsPerPage);
      params = params.append('Filter.firstname', checkUndefinedAndRemove(search.filter.firstname));
      params = params.append('Filter.lastname', checkUndefinedAndRemove(search.filter.lastname));
      params = params.append('Filter.policyNo', checkUndefinedAndRemove(search.filter.policyNo));
      params = params.append('Filter.memberNo', checkUndefinedAndRemove(search.filter.memberNo));
      params = params.append('Filter.insuredIdCard', checkUndefinedAndRemove(search.filter.insuredIdCard));
    }

    return this.http.get<Policie[]>(this.baseUrl + 'policies', { observe: 'response', params })
      .pipe(
        map(response => {
          paginatedResult.result = response.body;
          if (response.headers.get('Pagination') != null) {
            paginatedResult.pagination = JSON.parse(response.headers.get('Pagination'));
          }
          return paginatedResult;
        })
      );
  }

  getClaimsListForFollowUp(page: number, policyId: number, Search?: string): Observable<PaginatedResult<any[]>> {
    const paginatedResult: PaginatedResult<any[]> = new PaginatedResult<any[]>();
    let itemsPerPage = "10";
    let lang = localStorage.getItem("lang");
    let params = new HttpParams();


    if (page != null && itemsPerPage != null) {
      params = params.append('Pagination.PageNumber', page.toString());
      params = params.append('Pagination.PageSize', itemsPerPage);
    }
    if (Search != null) {
      params = params.append('Search', Search);
    }
    return this.http.get<any[]>(this.baseUrl + 'policies/' + policyId + '/claimsListForFollowUp', { observe: 'response', params })
      .pipe(
        map(response => {
          paginatedResult.result = response.body;
          if (response.headers.get('Pagination') != null) {
            paginatedResult.pagination = JSON.parse(response.headers.get('Pagination'));
          }
          return paginatedResult;
        })
      );
  }

}
