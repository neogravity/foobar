import {Injectable} from '@angular/core';
import {HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HTTP_INTERCEPTORS} from '@angular/common/http';
import { Observable } from 'rxjs';
import { OtherService } from './other.service';

@Injectable()
export class HeaderInterceptor implements HttpInterceptor {
  constructor(private otherService: OtherService) {}
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const clonedRequest = req.clone({ headers: req.headers.set('AppSetting', JSON.stringify(this.otherService.appSeting)) });
    return next.handle(clonedRequest);

  }
}

export const HeaderInterceptorProvider = {
  provide: HTTP_INTERCEPTORS,
  useClass: HeaderInterceptor,
  multi: true
};
