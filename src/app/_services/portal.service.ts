import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PortalService {
  baseUrl = environment.apiUrl + 'portalReserveClaim/';

  constructor(private http: HttpClient) { }

  getControlConfig(planId: number) {
    return this.http.get(this.baseUrl + 'getControlConfig/' + planId);
  }
}
