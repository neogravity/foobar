import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Router } from '@angular/router';
import { NbToastrService } from '@nebular/theme';
import { map } from 'rxjs/operators';
import { Policie } from '../_models/Policie';
import { PaginatedResult } from '../_models/pagination';
import { RequestListCriteriaDto } from '../DtoView/BaseDto/RequestListCriteriaDto';
import { Observable, Subject } from 'rxjs';
import { checkUndefinedAndRemove, checkUndefinedAndRemoveWithDate, checkUndefinedAndRemoveWithNumber } from '../_helpers/checkUndefinedAndRemove';
import { ClaimListFilterDto } from '../DtoView/claim-list/ClaimListFilterDto';
import { ClaimsInvoice } from '../_models/ClaimsInvoice';
import { ClaimReserveForBackendDto } from '../DtoView/create-claim/ClaimReserveForBackendDto';
import { ClaimUpdateForBackendDto } from '../DtoView/create-claim/ClaimUpdateForBackendDto';
import { FormGroup } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class ClaimService {
  baseUrl = environment.apiUrl + 'policy/';
  baseClaimUrl = environment.apiUrl + 'claim/';
  lang: string = localStorage.getItem('lang');

  constructor(private http: HttpClient, private router: Router, private toastrService: NbToastrService) { }

  claimsSearch(page: number, search: RequestListCriteriaDto<ClaimListFilterDto>) : Observable<PaginatedResult<ClaimsInvoice[]>> {
    const paginatedResult: PaginatedResult<ClaimsInvoice[]> = new PaginatedResult<ClaimsInvoice[]>();
    let params = new HttpParams();
    let itemsPerPage = "10";

    if (page != null && itemsPerPage != null) {

      params = params.append('Pagination.pageNumber', page.toString());
      params = params.append('Pagination.pageSize', itemsPerPage);
      params = params.append('Filter.firstname', checkUndefinedAndRemove(search.filter.firstname));
      params = params.append('Filter.lastname', checkUndefinedAndRemove(search.filter.lastname));
      params = params.append('Filter.policyNo', checkUndefinedAndRemove(search.filter.policyNo));
      params = params.append('Filter.memberNo', checkUndefinedAndRemove(search.filter.memberNo));
      params = params.append('Filter.gcl', checkUndefinedAndRemove(search.filter.gcl));
      params = params.append('Filter.caseNo', checkUndefinedAndRemove(search.filter.caseNo));
      params = params.append('Filter.statusId', checkUndefinedAndRemoveWithNumber(search.filter.statusId).toString());

      console.log(search.filter);

      if (search.filter.createdDateFrom != null && search.filter.createdDateTo != null){
        params = params.append('Filter.createdDateFrom', checkUndefinedAndRemoveWithDate(search.filter.createdDateFrom).toString());
        params = params.append('Filter.createdDateTo', checkUndefinedAndRemoveWithDate(search.filter.createdDateTo).toString());
      }

    }

    return this.http.get<ClaimsInvoice[]>(this.baseUrl + 'policies/claims', { observe: 'response', params })
      .pipe(
        map(response => {
          paginatedResult.result = response.body;
          if (response.headers.get('Pagination') != null) {
            paginatedResult.pagination = JSON.parse(response.headers.get('Pagination'));
          }
          return paginatedResult;
        })
      );
  }

  createClaimForBackend(req: ClaimReserveForBackendDto){
    return this.http.post(this.baseClaimUrl + 'reserveForBackend', req);
  }

  updateClaimForBackend(req: ClaimUpdateForBackendDto){
    return this.http.put(this.baseClaimUrl + 'reserveForBackend', req);
  }

}
