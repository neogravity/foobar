import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { AppSeting } from '../config/AppSeting';
import { Observable } from 'rxjs';
import { DiseaseItem } from '../_models/DiseaseItem';
import { DiseaseSearchParamsDto } from '../DtoView/claim-info/DiseaseSearchParamsDto';
import { forceCastArray } from '../_helpers/forceCast';
import { map } from 'rxjs/operators';
import { ExpenseItemSearchParamsDto } from '../DtoView/claim-info/ExpenseItemSearchParamsDto';
import { ResponseExpenseForExpenseItem } from '../DtoView/claim-info/ResponseExpenseForExpenseItem';
import { IaDto } from '../DtoView/claim-info/ExpenseGroup/IaDto';
import { RequestByExpenseItemDto } from '../DtoView/claim-info/RequestByExpenseItemDto';
import { RequestConfigClaimPortalChangeStatusDto } from '../DtoView/claim-info/RequestConfigClaimPortalChangeStatusDto';
import { HospitalSearchDto } from '../DtoView/claim-info/HospitalSearchDto';
import { Hospital } from '../_models/Hospital';
import { BackendUploadFileDto } from '../DtoView/claim-info/BackendUploadFileDto';

@Injectable({
  providedIn: 'root'
})
export class OtherService {
  baseUrl = environment.apiUrl + 'other/';

  appSeting: AppSeting = {
    appId: '1',
    partnerId: null
  };

  constructor(private http: HttpClient) { }

  getPartners(lang: string) {
    return this.http.get(this.baseUrl + 'getPartners/' + lang);
  }

  getDisease(icdSearch: string = null, icdType: number = null): Observable<DiseaseItem[]> {
    const icdSearchParam: DiseaseSearchParamsDto = new DiseaseSearchParamsDto();
    icdSearchParam.searchName = icdSearch;
    icdSearchParam.icdType = icdType;

    return this.http.post<DiseaseItem[]>(this.baseUrl + 'getDisease', icdSearchParam, { observe: 'response'})
      .pipe(
        map(response => {
          const diseaseItem = forceCastArray<DiseaseItem>(response.body);
          diseaseItem.map(c => {
            return c.code += ' - ' + c.diseaseItemLangs.find(f => f.language == localStorage.getItem('lang')).name;
          });
          return diseaseItem;
        })
      );
  }

  getHealthDiagnosised(){
    return this.http.get(this.baseUrl + 'getHealthDiagnosised');
  }

  getClaimTypeConcurrentProcesses(){
    return this.http.get(this.baseUrl + 'getClaimTypeConcurrentProcesses');
  }

  getDocType(){
    return this.http.get(this.baseUrl + 'getDocType');
  }

  // getExpenseItemById(beId){
  //   return this.http.get(this.baseUrl + 'getExpenseItemById/' + beId);
  // }

  getExpenseItemById(planId: number = null, iaId: number = null, beId: number = null, exId: number = null, hospitalId: number = null): Observable<IaDto[]> {
    const requestByExpenseItemDto: RequestByExpenseItemDto = new RequestByExpenseItemDto();
    requestByExpenseItemDto.planId = planId;
    requestByExpenseItemDto.iaId = iaId;
    requestByExpenseItemDto.beId = beId;
    requestByExpenseItemDto.exId = exId;
    requestByExpenseItemDto.hospitalId = hospitalId;

    return this.http.post<IaDto[]>(this.baseUrl + 'getExpenseItemById', requestByExpenseItemDto)
      .pipe(
        map((response) => {
          // const expenseItem = forceCast<ResponseExpenseForExpenseItem>(response);
          // expenseItem.map(c => {
          //   return c.expensesItemCode + ' - ' + c.expensesLangs.find(f => f.language == localStorage.getItem('lang')).name;
          // });
          
          return response;
        })
      );
  }

  getExpenseItemBySearch(expenseItemSearch: string = null, planId: number = null, iaId: number = null, hospitalId: number = null): Observable<ResponseExpenseForExpenseItem[]> {
    const expenseSearchParam: ExpenseItemSearchParamsDto = new ExpenseItemSearchParamsDto();
    expenseSearchParam.expenseItemSearch = expenseItemSearch;
    expenseSearchParam.planId = planId;
    expenseSearchParam.iaId = iaId;
    expenseSearchParam.hospitalId = hospitalId;

    return this.http.post<ResponseExpenseForExpenseItem[]>(this.baseUrl + 'getExpenseItemBySearch', expenseSearchParam)
      .pipe(
        map((response) => {
          return response;
        })
      );
  }

  getConfigClaimPortalChangeStatus(requestConfigClaimPortalChangeStatusDto: RequestConfigClaimPortalChangeStatusDto){

    let params = new HttpParams();
    params = params.append('partnerId', requestConfigClaimPortalChangeStatusDto.partnerId.toString());
    params = params.append('planTypeId', requestConfigClaimPortalChangeStatusDto.planTypeId.toString());
    params = params.append('claimsTypeId', requestConfigClaimPortalChangeStatusDto.claimsTypeId.toString());
    params = params.append('statusId', requestConfigClaimPortalChangeStatusDto.statusId.toString());
    params = params.append('lang.language', requestConfigClaimPortalChangeStatusDto.lang.language.toString());

    return this.http.get(this.baseUrl + 'getConfigClaimPortalChangeStatus', { params: params });
  }

  getHospital(hospitalSearch: string = null): Observable<Hospital[]> {
    const hospitalSearchParam: HospitalSearchDto = new HospitalSearchDto();
    hospitalSearchParam.search = hospitalSearch;
    hospitalSearchParam.lang = localStorage.getItem('lang');

    return this.http.post<Hospital[]>(this.baseUrl + 'getHospital', hospitalSearchParam, { observe: 'response'})
      .pipe(
        map(response => {
          const hospitalItem = forceCastArray<Hospital>(response.body);
          hospitalItem.map(c => {
            return c.hospitalsLangs.find(f => f.language == localStorage.getItem('lang')).name;
          });
          console.log(hospitalItem);
          return hospitalItem;
        })
      );
  }

  postDocumentForCreateDocument(claimsInvoiceId: number, backendUploadFileDto: BackendUploadFileDto[]){
    return this.http.post(this.baseUrl + 'postDocumentForCreateDocument/' + claimsInvoiceId, backendUploadFileDto);
  }

  getFileDocumentAttachments(claimsInvoiceId: number, fileName: string){
    return this.http.get(this.baseUrl + 'getFileDocumentAttachments/' + claimsInvoiceId + '/' + fileName, {responseType: 'blob'})
    .pipe(
      map((res) => {
          return new Blob([res], { type: res.type });
      })
    );
  }

}
