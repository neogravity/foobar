import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Router } from '@angular/router';
import { NbToastrService } from '@nebular/theme';
import { DashboardListConcurrentViewDto } from '../DtoView/dashboard/dashboard-list-concurrent-view-dto';
import { Observable, pipe } from 'rxjs';
import { map } from 'rxjs/operators';
import { DiseaseItem } from '../_models/DiseaseItem';
import { forceCast } from '../_helpers/forceCast';
import { DiseaseSearchParamsDto } from '../DtoView/claim-info/DiseaseSearchParamsDto';
import { ClaimsInvoiceDiagnosisedDto } from '../DtoView/dashboard/claims-invoice-diagnosised-dto';
import { BackendUploadFileDto } from '../DtoView/claim-info/BackendUploadFileDto';
import { RequestPostInfomationForClaimDto } from '../DtoView/claim-info/RequestPostInfomationForClaimDto';
import { InfomationClaim, InfomationForClaimDto } from '../DtoView/claim-info/Infomation-claim';
import { ResponseConfigClaimPortalChangeStatusDto } from '../DtoView/claim-info/ResponseConfigClaimPortalChangeStatusDto';

@Injectable({
  providedIn: 'root'
})
export class FaxclaimService {
  baseUrl = environment.apiUrl + 'partnerPortalFax/';
  lang: string = localStorage.getItem('lang');

  infomationForClaimDto: any = <any>{};
  dataBtnChangeStatus: ResponseConfigClaimPortalChangeStatusDto[] = [];

  constructor(private http: HttpClient, private router: Router, private toastrService: NbToastrService) { }

  async getPreAuthorizedForDashBoard() {
    let promise = new Promise((resolve, reject) => {
      this.http.get(this.baseUrl + 'getPreAuthorizedForDashBoard')
        .toPromise()
        .then(
          res => {
            resolve(res);
          },
          mes => {
            reject(mes);
          }
        );
    });
    return await promise;
  }

  async getConcurrentForDashBoard() {
    let promise = new Promise((resolve, reject) => {
      this.http.get(this.baseUrl + 'getConcurrentForDashBoard')
        .toPromise()
        .then(
          res => {
            resolve(res);
          },
          mes => {
            reject(mes);
          }
        );
    });
    return await promise;
  }

  async getDischargeForDashBoard() {
    let promise = new Promise((resolve, reject) => {
      this.http.get(this.baseUrl + 'getDischargeForDashBoard')
        .toPromise()
        .then(
          res => {
            resolve(res);
          },
          mes => {
            reject(mes);
          }
        );
    });
    return await promise;
  }

  async getToDoListForDashBoard() {
    let promise = new Promise((resolve, reject) => {
      this.http.get(this.baseUrl + 'getToDoListForDashBoard')
        .toPromise()
        .then(
          res => {
            resolve(res);
          },
          mes => {
            reject(mes);
          }
        );
    });
    return await promise;
  }

  getInfomationForClaim(claimsInvoiceId: number) {
    let params = new HttpParams();
    params = params.append('language', localStorage.getItem('lang'));

    return this.http.get(this.baseUrl + 'getInfomationForClaim/' + claimsInvoiceId, { params: params });
  }

  getInfomationForPolicy(policyId: number) {
    let params = new HttpParams();
    params = params.append('language', localStorage.getItem('lang'));

    return this.http.get(this.baseUrl + 'getInfomationForPolicy/' + policyId, { params: params });
  }

  postInfomationForClaim(requestPostInfomationForClaimDto: RequestPostInfomationForClaimDto) {
    let params = new HttpParams();
    params = params.append('language', localStorage.getItem('lang'));

    return this.http.post(this.baseUrl + 'postInfomationForClaim', requestPostInfomationForClaimDto, { params: params });
  }

  getConcurrentHistory(claimsInvoiceId: number) {
    return this.http.get(this.baseUrl + 'getConcurrentHistory/' + claimsInvoiceId);
  }

  postConcurrentHistory(dashboardListConcurrentViewDto: DashboardListConcurrentViewDto) {
    return this.http.post(this.baseUrl + 'postConcurrentHistory', dashboardListConcurrentViewDto);
  }

  getConcurrentHistoryPdfFile(claimsInvoiceId: number): any {
    let params = new HttpParams();
    params = params.append('lang.language', localStorage.getItem('lang'));

    return this.http.get(this.baseUrl + 'printConcurrentHistory/' + claimsInvoiceId, {responseType: 'blob', params: params})
    .pipe(
      map((res) => {
          return new Blob([res], { type: res.type });
      })
    )
  }

  getTreatmentList(){
    return this.http.get(this.baseUrl + 'getTreatmentList');
  }

  postClaimsInvoiceDiagnosised(claimsInvoiceDiagnosisedDto: ClaimsInvoiceDiagnosisedDto){
    return this.http.post(this.baseUrl + 'postClaimsInvoiceDiagnosised/' + this.lang, claimsInvoiceDiagnosisedDto);
  }

  getClaimsInvoiceDiagnosised(claimsInvoiceId: number){
    return this.http.get(this.baseUrl + 'getClaimsInvoiceDiagnosised/' + claimsInvoiceId + '/' + this.lang);
  }

  // postDocumentForCreateDocument(claimsInvoiceId: number, backendUploadFileDto: BackendUploadFileDto[]){
  //   return this.http.post(this.baseUrl + 'postDocumentForCreateDocument/' + claimsInvoiceId, backendUploadFileDto);
  // }

  getExpense(){
    return this.http.get(this.baseUrl + 'getExpense/' + this.lang);
  }

  postClaimNote(claimsInvoiceId: number, note: string){
    return this.http.post(this.baseUrl + 'postClaimNote/' + claimsInvoiceId, {note: note});
  }

  // getFileDocumentAttachments(claimsInvoiceId: number, fileName: string){
  //   return this.http.get(this.baseUrl + 'getFileDocumentAttachments/' + claimsInvoiceId + '/' + fileName, {responseType: 'blob'})
  //   .pipe(
  //     map((res) => {
  //         return new Blob([res], { type: res.type });
  //     })
  //   );
  // }

}
