import { ValidationErrors } from '@angular/forms';

export function getFormValidationErrors(form: any) : any[]{
    let errors = [];
    Object.keys(form.controls).forEach(key => {
        const controlErrors: ValidationErrors = form.get(key).errors;
        if (controlErrors != null) {
            Object.keys(controlErrors).forEach(keyError => {
                errors.push({
                    field: key.charAt(0).toUpperCase() + key.slice(1),
                    keyError: keyError.charAt(0).toUpperCase() + keyError.slice(1),
                    isError: controlErrors[keyError]
                });
            });
        }
    });

    return errors;
}