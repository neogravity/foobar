export function calPage({...param}, index: number) : number
{
    return ((param.currentPage - 1) * param.itemsPerPage) + (index + 1);
}