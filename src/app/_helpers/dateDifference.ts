import * as moment from 'moment';

export function dateDifference(startDate: any, endDate: any) : number{
    var a = moment(startDate,'MM/DD/YYYY');
    var b = moment(endDate,'MM/DD/YYYY');
    var diffDays = b.diff(a, 'days');

    return diffDays;
}
