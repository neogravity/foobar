
export function forceCastArray<T>(input: any): Array<T> {
    return input;
}

export function forceCast<T>(input: any): T {
    return input;
}
