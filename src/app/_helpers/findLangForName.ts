export function findLangForName(input: any[]) : any{
    return input.find(f => f.language == localStorage.getItem('lang'));
}
