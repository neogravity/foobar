import * as moment from 'moment';

export function checkUndefinedAndRemove(input: any){
    if (typeof input == 'undefined' || input == '' || input == null)
        return '';
    return input;
}

export function checkUndefinedAndRemoveWithNumber(input: any) : number{
    if (typeof input == 'undefined' || input == '' || input == null)
        return -1;
    return input;
}

export function checkUndefinedAndRemoveWithDate(input: Date) : string{
    if (typeof input == 'undefined' || input == null)
        return null;
    return moment(input).format('L');
}