import { Injectable } from '@angular/core';
import { GlobalNotification } from '../DtoView/nav-header/global-notification';

@Injectable({
  providedIn: 'root'
})
export class GlobalService {
  globalNotification: GlobalNotification;

  constructor() {
    this.globalNotification = {
      countNotification: 5,
      detailListNotification: []
    };

    this.globalNotification.detailListNotification = [
      {
        gcl: 'GCL-MTI0519-00001',
        status: 'Pre-Authorized',
        iconPartner: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRvL_7UT1a-MqehNTPSdpCCxS4veXL7ZYUF_r9pG5FHyy6YxQhj'
      },
      {
        gcl: 'GCL-MTI0519-00002',
        status: 'Discharge',
        iconPartner: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQU-8Oh92esD9BZ3M-Sx-M6iBeW-9Q7M3GtdH3h05S3-bDBFR1A'
      },
      {
        gcl: 'GCL-MTI0519-00003',
        status: 'Pre-Authorized',
        iconPartner: 'http://www.lawtonasia.com/wp-content/themes/lawtonasia/images/logo.png'
      },
      {
        gcl: 'GCL-MTI0519-00004',
        status: 'Discharge',
        iconPartner: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQU-8Oh92esD9BZ3M-Sx-M6iBeW-9Q7M3GtdH3h05S3-bDBFR1A'
      },
      {
        gcl: 'GCL-MTI0519-00003',
        status: 'Discharge',
        iconPartner: 'http://www.lawtonasia.com/wp-content/themes/lawtonasia/images/logo.png'
      },
    ];
  }

  addGlobalNotification() {
    this.globalNotification.countNotification++;
    this.globalNotification.detailListNotification.push({
      gcl: 'GCL-MTI0519-0000' + this.globalNotification.countNotification,
      status: 'Discharge',
      iconPartner: 'http://www.lawtonasia.com/wp-content/themes/lawtonasia/images/logo.png'
    });
  }

}
