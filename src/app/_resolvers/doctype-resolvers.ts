import { Injectable } from '@angular/core';
import { Resolve, Router, ActivatedRouteSnapshot } from '@angular/router';
import { AlertifyService } from '../_services/alertify.service';
import { OtherService } from '../_services/other.service';

@Injectable()
export class DoctypeResolver implements Resolve<any> {
    lang: string = localStorage.getItem("lang");
    constructor(private otherService: OtherService, private router: Router,
        private alertify: AlertifyService) { }

    resolve(route: ActivatedRouteSnapshot) {
        return this.otherService.getDocType();
    }
}
