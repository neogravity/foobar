interface DetailGlobalNotification{
    iconPartner: string;
    gcl: string;
    status: string;
}

export interface GlobalNotification {
    countNotification: number;
    detailListNotification: Array<DetailGlobalNotification>;
}
