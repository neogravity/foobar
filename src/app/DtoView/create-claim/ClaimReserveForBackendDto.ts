export class ClaimReserveForBackendDto {
    policy: ObjectRefDto;
    hospital: ObjectRefDto;
    insuringAgreement: ObjectRefDto;
    claimType: ObjectRefDto;
    accidentDate: Date;
    invoiceDate: Date;
    visitDate: Date;
    admitDate: Date;
    dischargeDate: Date;
    receivedDate: Date;
    claimTypeConcurrent: number;
    invoiceNo: string;
    hospitalNo: string;
    relatedGcl: string;
    documentType: ObjectRefDto;
    remark: string;
    expenses: number;
    discount: number;
    claimProcess: ObjectRefDto;
}

export class ObjectRefDto{
    id: number;
}