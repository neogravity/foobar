export class PolicyFilterDto {
    id: number = null;
    partnerId: number = null;
    policyNo: string = '';
    memberNo: string = '';
    firstname: string = '';
    lastname: string = '';
    insuredIdCard: string = '';
    policyCard: string = '';
}
