import { ClaimReserveForBackendDto, ObjectRefDto } from './ClaimReserveForBackendDto';

export class ClaimUpdateForBackendDto extends ClaimReserveForBackendDto {
    claimsInvoice: ObjectRefDto;
    status?: ObjectRefDto;
}
