export class PaginationInputDto
{
    pageNumber: number = 1;
    pageSize: number;
}