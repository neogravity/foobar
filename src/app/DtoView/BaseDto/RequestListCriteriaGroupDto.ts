import { PaginationInputDto } from './PaginationInputDto';
import { Sort } from './Sort';

export class RequestListCriteriaGroupDto
{
    search: string;
    sort: Sort<SortKeyDefault>;
    pagination: PaginationInputDto;
    optionLang: string;
}