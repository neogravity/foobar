import { RequestListCriteriaGroupDto } from './RequestListCriteriaGroupDto';
import { Sort } from './Sort';

export class RequestListCriteriaDto<T> extends RequestListCriteriaGroupDto
{
    filter: T = <T>{};
    sort: Sort<SortKeyDefault>;
}


