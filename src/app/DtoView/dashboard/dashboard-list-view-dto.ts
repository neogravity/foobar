import { DashboardActionViewDto } from './dashboard-action-view-dto';

export interface DashboardListViewDto {
    gcl: string;
    claimType: string;
    chanel: string;
    action?: Array<DashboardActionViewDto>;
}
