import { DashboardListViewDto } from './dashboard-list-view-dto';

export interface DashboardViewDto {
    title: string;
    dashboardLists: Array<DashboardListViewDto>;
}
