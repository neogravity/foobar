export interface DashboardListConcurrentViewDto {
    claimsInvoiceId: number;
    gcl: string;
    firstName: string;
    lastName: string;
    dateLog: Date;
    disease: string;
    operationDate: Date;
    operativeProcedure: string;
    problemPrognosis: string;
    investigation: string;
    orderForOneday: string;
    orderForContinue: string;
    planDC: string;
    expenses: number;
}
