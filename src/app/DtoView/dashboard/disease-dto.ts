export interface DiseaseDto {
    id: number;
    primary: boolean;
    used: boolean;
}
