import { DashboardListViewDto } from './dashboard-list-view-dto';

export interface DashBoardPreAuthorizedDto {
    claimsInvoiceId: number;
    gcl: string;
    firstName: string;
    lantName: string;
    hospitalName: string;
    insuranceCompanyName: string;
    claimType: string;
    requestDate: Date;
}
