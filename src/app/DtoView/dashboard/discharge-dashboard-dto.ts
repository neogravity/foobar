export interface DischargeDashboardDto {
    claimsInvoiceId: number;
    gcl: string;
    firstName: string;
    lantName: string;
    policyNumber: string;
    hospitalName: string;
    admitDate: Date;
    requestDate: Date;
    admitDateCount: number;
    claimType: string;
    dateTimeRequest: Date;
}
