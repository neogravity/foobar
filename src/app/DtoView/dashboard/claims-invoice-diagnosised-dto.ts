import { DiseaseDto } from './disease-dto';

export interface ClaimsInvoiceDiagnosisedDto {
    claimsInvoiceId: number;
    diseaseICD10: Array<DiseaseDto>;
    diseaseICD9: Array<DiseaseDto>;
    healthDiagnosisId: number;
    treatmentId: number;
}
