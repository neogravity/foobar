export interface TodoListDashboardDto {
    claimsInvoiceId: number;
    gcl: string;
    dateMadeTodo: Date;
    claimType: string;
    description: string;
}
