export interface ConcurrentDashboardDto {
    claimsInvoiceId: number;
    gcl: string;
    firstName: string;
    lantName: string;
    admitDate: Date;
    admitDateCount: number;
    expenses: string;
}
