export interface DashboardActionViewDto {
    actionItemName: string;
    event: Function;
    icon: string;
}
