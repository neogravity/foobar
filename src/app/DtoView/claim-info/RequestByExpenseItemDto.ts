export class RequestByExpenseItemDto {
    planId: number;
    hospitalId: number;
    iaId: number;
    beId: number;
    exId: number;
}
