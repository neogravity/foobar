export interface BackendUploadFileDto {
    fileNameOriginal: string;
    fileName: string;
}
