export interface ResponseConfigClaimPortalChangeStatusDto {
    statusId: number;
    statusCode: string;
    name: string;
    colorBtn: string;
    iconBtn: string;
}
