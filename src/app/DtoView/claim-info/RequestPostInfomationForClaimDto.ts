import { ClaimUpdateForBackendDto } from '../create-claim/ClaimUpdateForBackendDto';

export interface RequestPostInfomationForClaimDto {
    claimsInvoiceId: number;
    statusId: number;
    claimUpdateForBackendDto: ClaimUpdateForBackendDto;
}
