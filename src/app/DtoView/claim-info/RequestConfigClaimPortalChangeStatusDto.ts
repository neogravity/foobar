import { RequestLangDto } from '../BaseDto/RequestLangDto';

export interface RequestConfigClaimPortalChangeStatusDto {
    partnerId: number;
    planTypeId: number;
    claimsTypeId: number;
    statusId: number;
    lang: RequestLangDto;
}
