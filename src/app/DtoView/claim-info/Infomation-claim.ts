import { Lang } from '../../_models/Lang';
import { ClaimsInvoice } from '../../_models/ClaimsInvoice';
import { Plan } from '../../_models/Plan';

export interface InfomationForPolicy{
    policyNo: string;
    memberNo: string;
    cardNo: string;
    gcl: string;
    memberStatus: string;
    firstName: string;
    lastName: string;
    memberEffectiveDate: Date;
    memberEndDate: Date;
}

export interface InfomationForClaimDto {
    infomationHospitals: InfomationHospital;
    infomationPlans: InfomationPlan;
    infomationPlanInsuringAgreements: InfomationPlanInsuringAgreement;
    infomationClaims: InfomationClaim;
    infomationForPolicys: InfomationForPolicy;
}

export interface InfomationHospital{
    hospitalId: number;
    hospitalLangs: Array<Lang>;
}

export interface InfomationPlan{
    planId: number;
    plan: any;
}

export interface InfomationPlanInsuringAgreement{
    planInsuringAgreementId: number;
}

export interface InfomationClaim extends ClaimsInvoice{
    docTypeLang: string;
    statusClaimLang: string;
}
