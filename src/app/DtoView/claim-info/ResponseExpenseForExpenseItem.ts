import { ExpensesLang } from '../../_models/ExpensesLang';

export interface ResponseExpenseForExpenseItem {
    // iaId: number;
    // coId: number;
    beId: number;
    exId: number;
    discount: number;
    expensesItemCode: string;
    expensesLangs: Array<ExpensesLang>;
}
