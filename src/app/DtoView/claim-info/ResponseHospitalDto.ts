import { Hospital } from '../../_models/Hospital';

export class ResponseHospitalDto extends Hospital {
    name: string;
}
