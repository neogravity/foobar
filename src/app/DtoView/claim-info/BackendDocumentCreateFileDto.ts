export interface BackendDocumentCreateFileDto {
    fileNameOriginal: string;
    fileName: string;
}
