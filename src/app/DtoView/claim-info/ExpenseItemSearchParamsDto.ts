export class ExpenseItemSearchParamsDto {
    expenseItemSearch: string;
    hospitalId: number;
    planId: number;
    iaId: number;
}
