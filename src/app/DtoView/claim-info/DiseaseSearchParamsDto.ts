// export interface HospitalPortalSearchParamsDto {
//     partnerId: number;
//     policyNumber: string;
//     memberNo: string;
//     insuredName: string;
//     insuredSurname: string;
//     idCard: string;
//     cardNo: string;
// }

export class DiseaseSearchParamsDto {
    searchName: string;
    icdType: number;
}
