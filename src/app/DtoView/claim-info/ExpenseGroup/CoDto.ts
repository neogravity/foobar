import { Lang } from './Lang';
import { BeDto } from './BeDto';

export interface CoDto {
    id: number;
    code: string;
    benefitDescription: string;
    coveragesLangs: Array<Lang>;
    beDtos: Array<BeDto>;
}
