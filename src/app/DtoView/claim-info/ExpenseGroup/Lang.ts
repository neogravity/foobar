export interface Lang {
    id: number;
    language: string;
    name: string;
}
