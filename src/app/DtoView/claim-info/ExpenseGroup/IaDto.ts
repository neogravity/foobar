import { Lang } from './Lang';
import { CoDto } from './CoDto';

export interface IaDto {
    id: number;
    code: string;
    benefitDescription: string;
    insuringAgreementLangs: Array<Lang>;
    coDtos: Array<CoDto>;
}
