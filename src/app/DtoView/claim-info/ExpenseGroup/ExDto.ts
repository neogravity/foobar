import { Lang } from './Lang';

export interface ExDto {
    id: number;
    expensesItemCode: string;
    discount: number;
    expensesLangs: Array<Lang>;
}
