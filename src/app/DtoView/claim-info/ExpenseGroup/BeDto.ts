import { Lang } from './Lang';
import { ExDto } from './ExDto';

export interface BeDto {
    id: number;
    code: string;
    benefitDescription: string;
    benefitLangs: Array<Lang>;
    exDtos: Array<ExDto>;
}
