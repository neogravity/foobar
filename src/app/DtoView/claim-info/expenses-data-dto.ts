export class ia {
    id: number;
    itemName: string;
    description: string;
    expense: number;
    discount: number;
    total: number;
    coPayment: number;
    cover: number;
    uncover: number;
    deductible: number;
    isCollapsed: boolean;

    co: Array<co>;
}

export class co {
    id: number;
    itemName: string;
    description: string;
    expense: number;
    discount: number;
    total: number;
    coPayment: number;
    cover: number;
    uncover: number;
    deductible: number;
    isCollapsed: boolean;
    
    be: Array<be>;
}

export class be {
    id: number;
    itemName: string;
    description: string;
    expense: number;
    discount: number;
    total: number;
    coPayment: number;
    cover: number;
    uncover: number;
    deductible: number;
    isCollapsed: boolean;

    ex: Array<ex>;
}

export class ex {
    id: number;
    itemName: string;
    expensesUnit: number;
    unit: number;
    discountMod: number;
    hpDiscount: number;
    reject: number;
    isReject: boolean;

    expensesTotal: number;
    defaultExpensesTotal: number;
    discount: number;
    total: number;
}

export class ExpensesDataDto {
    ia: ia;
}
