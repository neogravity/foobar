export class ClaimListFilterDto {
    id: number = null;
    partnerId: number = null;
    policyNo: string = '';
    memberNo: string = '';
    firstname: string = '';
    lastname: string = '';
    gcl: string = '';
    caseNo: string = '';
    statusId: number = null;
    createdDateFrom: Date;
    createdDateTo: Date;
}
