import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { AuthService } from '../_services/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertifyService } from '../_services/alertify.service';
import { Router } from '@angular/router';
import { User } from '../_models/User';
import { SpinnerService } from '../_services/spinner.service';
import { NbToastrService, NbIconConfig, NbGlobalPositionStrategy, NbGlobalLogicalPosition } from '@nebular/theme';

@Component({
  selector: 'ngx-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  user: User;
  loginForm: FormGroup;

  constructor(private spinner: SpinnerService, private authService: AuthService, private fb: FormBuilder, private toastrService: NbToastrService, private router: Router) { }

  ngOnInit() {
    this.loginForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });

    if (this.authService.loggedIn()) {
      this.router.navigate(['/component']);
    } else {
      localStorage.removeItem('token');
      localStorage.removeItem('user');
      this.authService.decodedToken = null;
      this.authService.currentUser = null;
      this.router.navigate(['/login']);
    }
  }

  login() {
    if (this.loginForm.valid) {
      this.user = Object.assign({}, this.loginForm.value);
      this.spinner.spinnerShow();
      this.authService.login(this.user).subscribe(next => {
      }, error => {
        this.spinner.spinnerHide();
        this.toastrService.show(null, error, {status: 'danger', position: NbGlobalLogicalPosition.BOTTOM_END});
      }, () => {
        this.router.navigate(['/component']);
      });
    }
  }

}
