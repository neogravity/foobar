import { NgModule } from '@angular/core';
import { FocusDirective } from '../_directives/focus.directive';
import { NumericDirective } from '../_directives/numeric.directive';

@NgModule({
    declarations: [
        NumericDirective,
        FocusDirective
    ],
    imports: [
        
    ],
    exports: [
        NumericDirective,
        FocusDirective
    ],
    entryComponents: [],
    providers: []
})
export class DirectiveModule { }
