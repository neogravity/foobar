import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NbTooltipModule, NbBadgeModule, NbMenuModule, NbCardModule, NbIconModule } from '@nebular/theme';
import { DashboardFaxCardComponent } from '../components/FAX-CLAIM/dashboard-fax/dashboard-fax-card/dashboard-fax-card.component';
import { ThemeModule } from '../@theme/theme.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ConcurrentDashboardComponent } from '../components/FAX-CLAIM/dashboard-fax/concurrent-dashboard/concurrent-dashboard.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { MomentModule } from 'ngx-moment';
import { NgSelectModule } from '@ng-select/ng-select';
import { MatPaginatorModule, MatRadioModule, MatCheckboxModule } from '@angular/material';

@NgModule({
    declarations: [
        DashboardFaxCardComponent,
        ConcurrentDashboardComponent
    ],
    imports: [
        CommonModule,
        NbTooltipModule,
        NbBadgeModule,
        ThemeModule,
        NbMenuModule,
        NbCardModule,
        ReactiveFormsModule,
        FormsModule,
        Ng2SearchPipeModule,
        MomentModule,
        NgSelectModule,
        NbIconModule,
        MatPaginatorModule,
        MatRadioModule,
        MatCheckboxModule
    ],
    exports: [
        CommonModule,
        NbTooltipModule,
        NbBadgeModule,
        ThemeModule,
        NbMenuModule,
        NbCardModule,
        ReactiveFormsModule,
        FormsModule,
        Ng2SearchPipeModule,
        MomentModule,
        NgSelectModule,
        NbIconModule,
        MatPaginatorModule,
        MatRadioModule,
        MatCheckboxModule,

        DashboardFaxCardComponent,
        ConcurrentDashboardComponent
    ],
    entryComponents: [],
    providers: []
})
export class SharedModule { }
