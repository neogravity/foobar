import { Component, OnDestroy, OnInit } from '@angular/core';
import { NbMediaBreakpointsService, NbMenuService, NbSidebarService, NbThemeService } from '@nebular/theme';

import { UserData } from '../../../@core/data/users';
import { LayoutService } from '../../../@core/utils';
import { map, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { filter } from 'minimatch';
import { AuthService } from '../../../_services/auth.service';
import { Router } from '@angular/router';
import { HeaderService } from '../../../_services/header.service';
import { GlobalService } from '../../../_service-inapp/global.service';

@Component({
  selector: 'ngx-header',
  styleUrls: ['./header.component.scss'],
  templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit, OnDestroy {
  private destroy$: Subject<void> = new Subject<void>();
  userPictureOnly: boolean = false;
  user: any;

  currentTheme = 'default';

  userMenu = [ { title: 'Log out', icon: 'log-out-outline' } ];

  constructor(private sidebarService: NbSidebarService,
              private themeService: NbThemeService,
              private userService: UserData,
              private layoutService: LayoutService,
              private menuService: NbMenuService,
              private authService: AuthService,
              public headerService: HeaderService,
              public globalService: GlobalService,
              private breakpointService: NbMediaBreakpointsService) {
  }

  ngOnInit() {
    this.user = {
      name: this.authService.currentUser.firstName.toUpperCase() + ' ' + this.authService.currentUser.lastName.toUpperCase(),
      picture: './assets/imageapp/person-svg-png-icon-person.png'
    };

    this.menuService.onItemClick()
      .subscribe((event) => {
        this.onContecxtItemSelection(event.item.title);
      });
  }

  onContecxtItemSelection(title) {
    if ('Log out' == title){
      this.authService.logout();
    }
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  changeTheme(themeName: string) {
    this.themeService.changeTheme(themeName);
  }

  toggleSidebar(): boolean {
    this.sidebarService.toggle(true, 'menu-sidebar');
    this.layoutService.changeLayoutSize();

    return false;
  }
}
