import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';
import { AlertifyService } from '../../../../../../_services/alertify.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { SpinnerService } from '../../../../../../_services/spinner.service';
import { TranslateService } from '@ngx-translate/core';
import { Subject } from 'rxjs/Subject';
import { PolicyService } from '../../../../../../_services/policy.service';
import { finalize } from 'rxjs/operators';
import { findLangForName } from '../../../../../../_helpers/findLangForName';
import { Pagination, PaginatedResult } from '../../../../../../_models/pagination';

@Component({
  selector: 'ngx-related-gcl',
  templateUrl: './related-gcl.component.html',
  styleUrls: ['./related-gcl.component.scss']
})
export class RelatedGclComponent implements OnInit {
  currentPage = 1;
  page: number;
  totalItems: number;
  pagination: Pagination;
  claimsInvoice: any[] = [];
  filterList = { Search: "" };
  
  public onSelectRelateGcl: Subject<string> = new Subject<string>();
  findLangForName: Function = findLangForName;
  item: any;

  constructor(private policyService: PolicyService, public bsModalRef: BsModalRef, private alertify: AlertifyService, private route: ActivatedRoute,
    private fb: FormBuilder, private spinner: SpinnerService, private translateService: TranslateService, private router: Router) { }

  ngOnInit() {
    this.getClaimsListForFollowUp(1);
  }

  getClaimsListForFollowUp(page: number, search?: string){
    this.spinner.spinnerShow();
    this.policyService.getClaimsListForFollowUp(page, this.item.policyId, search)
    .pipe(
      finalize(() => this.spinner.spinnerHide())
    )
    .subscribe((res: PaginatedResult<any[]>) => {
      this.claimsInvoice = res.result;
      this.pagination = res.pagination;
      this.totalItems = res.pagination.totalItems;
    });
  }

  btnRelate(item: any){
    this.onSelectRelateGcl.next(item.gcl);
    this.bsModalRef.hide();
  }

  btnReset(){
    this.onSelectRelateGcl.next('');
    this.bsModalRef.hide();
  }

  pageChanged(event: any): void {
    this.page = event.page;
    this.getClaimsListForFollowUp(this.page, this.filterList.Search);
  }

  fnSearch() {
    this.getClaimsListForFollowUp(1, this.filterList.Search);
  }

}
