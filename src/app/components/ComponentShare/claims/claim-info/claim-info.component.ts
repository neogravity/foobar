import { Component, OnInit, ViewChild } from '@angular/core';
import { DashboardViewDto } from '../../../../DtoView/dashboard/dashboard-view-dto';
import { FaxclaimService } from '../../../../_services/faxclaim.service';
import { NbToastrService, NbGlobalLogicalPosition, NbTabsetComponent, NbTabComponent } from '@nebular/theme';
import { DashboardListViewDto } from '../../../../DtoView/dashboard/dashboard-list-view-dto';
import { DashboardActionViewDto } from '../../../../DtoView/dashboard/dashboard-action-view-dto';
import { ModalService } from '../../../FAX-CLAIM/modal.service';
import { Router, ActivatedRoute } from '@angular/router';
import { InfomationClaim, InfomationForClaimDto } from '../../../../DtoView/claim-info/Infomation-claim';
import { AlertifyService } from '../../../../_services/alertify.service';
import { SpinnerService } from '../../../../_services/spinner.service';
import { finalize, debounceTime, distinctUntilChanged, tap, switchMap, catchError } from 'rxjs/operators';
import { DataService } from '../../../FAX-CLAIM/data.service';
import { DashboardListConcurrentViewDto } from '../../../../DtoView/dashboard/dashboard-list-concurrent-view-dto';
import * as moment from 'moment';
import { Treatment } from '../../../../_models/Treatment';
import { NgSelectConfig } from '@ng-select/ng-select';
import { Observable, Subject, concat, of } from 'rxjs';
import { DiseaseItem } from '../../../../_models/DiseaseItem';
import { OtherService } from '../../../../_services/other.service';
import { HealthDiagnosis } from '../../../../_models/HealthDiagnosis';
import { ClaimsInvoiceDiagnosisedDto } from '../../../../DtoView/dashboard/claims-invoice-diagnosised-dto';
import { DiseaseDto } from '../../../../DtoView/dashboard/disease-dto';
import { FileUploader } from 'ng2-file-upload';
import { environment } from '../../../../../environments/environment';
import { BackendDocumentCreateFileDto } from '../../../../DtoView/claim-info/BackendDocumentCreateFileDto';
import { BackendUploadFileDto } from '../../../../DtoView/claim-info/BackendUploadFileDto';
import { ExpensesDataDto } from '../../../../DtoView/claim-info/expenses-data-dto';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { ResponseConfigClaimPortalChangeStatusDto } from '../../../../DtoView/claim-info/ResponseConfigClaimPortalChangeStatusDto';
import { Hospital } from '../../../../_models/Hospital';
import { PortalService } from '../../../../_services/portal.service';
import { findLangForName } from '../../../../_helpers/findLangForName';
import { BsDatepickerConfig } from 'ngx-bootstrap';
import { ClaimService } from '../../../../_services/claim.service';
import { ClaimReserveForBackendDto, ObjectRefDto } from '../../../../DtoView/create-claim/ClaimReserveForBackendDto';
import { ClaimModalService } from '../claim-modal.service';
import { ClaimUpdateForBackendDto } from '../../../../DtoView/create-claim/ClaimUpdateForBackendDto';
import { getFormValidationErrors } from '../../../../_helpers/getFormValidationErrors';

@Component({
  selector: 'ngx-claim-info',
  templateUrl: './claim-info.component.html',
  styleUrls: ['./claim-info.component.scss']
})
export class ClaimInfoComponent implements OnInit {
  @ViewChild("tabset", { static: false }) tabsetEl: NbTabsetComponent;
  @ViewChild("claimInfo", { static: false }) claimInfoTabEl: NbTabComponent;

  findLangForName: Function = findLangForName;
  isGroupProcess: boolean = true;
  isEdit: boolean = true;
  claimNote: string = '';

  bsConfig: Partial<BsDatepickerConfig>;
  colorTheme = 'theme-dark-blue';

  claimTypeList = [];
  selectedClaimTypeItems = [];

  insuringAgreementList = [];
  selectedInsuringAgreementItems = [];

  isProcess: boolean = false;
  processList = [];
  selectedProcessItems = [];

  claimTypeConcurrentProcess = [];
  documentType = [];

  dropdownSettings = {};

  getClaimType: any = {};

  disabledTab: boolean = true;

  uploader: FileUploader;
  backendUploadFileDto: BackendUploadFileDto[] = [];

  treatment: Treatment[] = [];
  healthDiagnosis: HealthDiagnosis[] = [];
  selectHealthDiagnosis: number = 1;
  selectTreatments: number = 1;

  concurrentSearch: string;
  claimsInvoiceId: number;
  policyId: number;

  selectedClaimTypeConcurrent: string;
  selectedDocumentType: string;
  selectedInsuredPaymentMethod: string = '1';
  selectedPayInName: string = '1';

  diseaseItem10$: Observable<DiseaseItem[]>;
  diseaseLoading10 = false;
  diseaseInput10$ = new Subject<string>();
  selectedDiseases10: DiseaseItem[];
  diseases10: any[] = <any>[];
  selectDiseases10Primary: any;

  diseaseItem9$: Observable<DiseaseItem[]>;
  diseaseLoading9 = false;
  diseaseInput9$ = new Subject<string>();
  selectedDiseases9: DiseaseItem[];
  diseases9: any[] = <any>[];
  selectDiseases9Primary: any;

  hospitalItem$: Observable<Hospital[]>;
  hospitalLoading = false;
  hospitalInput$ = new Subject<string>();
  selectedHospital: Hospital;
  hospital: any[] = <any>[];

  claimsInvoiceDiagnosisedDto: ClaimsInvoiceDiagnosisedDto = <ClaimsInvoiceDiagnosisedDto>{};
  dataDetailInvoice: any = <any>{};

  claimInfoForm: FormGroup;

  constructor(private claimModalService: ClaimModalService, private claimService: ClaimService, private portalService: PortalService, public otherService: OtherService, private fb: FormBuilder, private config: NgSelectConfig, public data: DataService, private spinner: SpinnerService, private alertify: AlertifyService, public faxclaimService: FaxclaimService, private toastrService: NbToastrService, public modal: ModalService, private router: Router, private route: ActivatedRoute) {
    this.config.notFoundText = 'ICD not found';

    this.dropdownSettings = {
      singleSelection: true,
      idField: 'id',
      textField: 'name',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true,
      closeDropDownOnSelection: true
    };

    this.route.data.subscribe((data) => {
      const claimTypeConcurrentProcessResolver = data["ClaimTypeConcurrentProcessResolver"];
      const doctypeResolver = data["DoctypeResolver"];
      console.log(doctypeResolver);

      let claimTypeConcurrentProcess = [];
      claimTypeConcurrentProcessResolver.map(m => {
        claimTypeConcurrentProcess.push({
          id: findLangForName(m.claimTypeConcurrentProcessLangs).id,
          name: findLangForName(m.claimTypeConcurrentProcessLangs).name
        });
      });
      this.claimTypeConcurrentProcess = claimTypeConcurrentProcess;


      let documentType = [];
      doctypeResolver.map(m => {
        documentType.push({
          id: findLangForName(m.docTypeLangs).id,
          name: findLangForName(m.docTypeLangs).name
        });
      });
      this.documentType = documentType;

    });

    this.bsConfig = Object.assign({}, {
      containerClass: this.colorTheme,
      dateInputFormat: 'DD-MM-YYYY h:mm:ss',
    });
  }

  async ngOnInit() {

    this.faxclaimService.getExpense().subscribe((res: ExpensesDataDto) => {
      this.data.expensessData1 = res;
    });

    this.loadDisease10Item();
    this.loadDisease9Item();
    this.loadHospital();

    this.claimsInvoiceId = parseInt(this.route.snapshot.paramMap.get('claimsInvoiceId'));
    this.policyId = parseInt(this.route.snapshot.paramMap.get('policyId'));

    if (!isNaN(this.claimsInvoiceId)) {
      this.getClaimInfomation();


      this.spinner.spinnerShow();
      this.data.dashboardListConcurrentViewDto = [];
      this.faxclaimService.getConcurrentHistory(this.claimsInvoiceId)
        .pipe(
          finalize(() => this.spinner.spinnerHide())
        )
        .subscribe((res: DashboardListConcurrentViewDto[]) => {
          this.data.dashboardListConcurrentViewDto = res;
        }, error => {
          this.toastrService.danger(error, "Error", { status: 'primary', position: NbGlobalLogicalPosition.BOTTOM_END });
        });

      this.spinner.spinnerShow();
      this.faxclaimService.getClaimsInvoiceDiagnosised(this.claimsInvoiceId)
        .pipe(
          finalize(() => this.spinner.spinnerHide())
        ).subscribe((res: any) => {
          this.diseases9 = res.diseaseICD9;
          this.diseases10 = res.diseaseICD10;
          this.selectHealthDiagnosis = res.healthDiagnosisId;
          this.selectTreatments = res.treatmentId;
        });

    } else if (!isNaN(this.policyId)) {
      this.isGroupProcess = false;
      this.isEdit = false;
      this.spinner.spinnerShow();
      this.faxclaimService.getInfomationForPolicy(this.policyId)
        .pipe(
          finalize(() => this.spinner.spinnerHide())
        )
        .subscribe((res: InfomationForClaimDto) => {
          this.faxclaimService.infomationForClaimDto = res;
          this.faxclaimService.dataBtnChangeStatus = [];

          this.getControlConfig(res, () => { });

        }, error => {
          this.toastrService.danger(error, "Error", { status: 'primary', position: NbGlobalLogicalPosition.BOTTOM_END });
        });
    }


    this.spinner.spinnerShow();
    this.faxclaimService.getTreatmentList()
      .pipe(
        finalize(() => this.spinner.spinnerHide())
      )
      .subscribe((res: Treatment[]) => {
        this.treatment = res;
      }, error => {
        this.toastrService.danger(error, "Error", { status: 'primary', position: NbGlobalLogicalPosition.BOTTOM_END });
      });

    this.spinner.spinnerShow();
    this.otherService.getHealthDiagnosised()
      .pipe(
        finalize(() => this.spinner.spinnerHide())
      )
      .subscribe((res: HealthDiagnosis[]) => {
        console.log(res);
        this.healthDiagnosis = res;
      }, error => {
        this.toastrService.danger(error, "Error", { status: 'primary', position: NbGlobalLogicalPosition.BOTTOM_END });
      });

    //Upload File

    this.uploader = new FileUploader({
      url: environment.apiUrl + 'other/uploadFileForCreateDocument',
      authToken: 'Bearer ' + localStorage.getItem('token'),
      isHTML5: true,
      allowedFileType: ['image', 'pdf', 'xls', 'xlsx', 'doc', 'docx'],
      removeAfterUpload: true,
      autoUpload: false,
      maxFileSize: 10 * 1024 * 1024,
      headers: [{ name: 'AppSetting', value: '{appId: 1, partnerId: null}' }]
    });

    this.uploader.onBeforeUploadItem = (item) => {
      item.withCredentials = false;
    }

    this.uploader.onWhenAddingFileFailed = (item) => {
      this.toastrService.danger("This file extension is " + item.name + " incorrect.", "Error", { status: 'primary', position: NbGlobalLogicalPosition.BOTTOM_END })
    }

    this.uploader.onSuccessItem = (item, response, status, headers) => {
      if (response) {
        const res: BackendDocumentCreateFileDto = JSON.parse(response);
        this.backendUploadFileDto.push({ fileNameOriginal: res.fileNameOriginal, fileName: res.fileName });
      }
    };

    this.uploader.onCompleteAll = () => {
      this.uploadFileDocument();
    }

    //End Config File

    this.claimInfoForm = this.fb.group({
      claimTypeConcurrent: [null],
      claimType: [null, Validators.required],
      claimProcess: [null],
      insuringAgreement: [null, Validators.required],
      hospital: [null, Validators.required],
      invoiceDate: [null],
      invoiceNo: [null],
      receivedDate: [null],
      accidentDate: [null, Validators.required],
      admitDate: [null],
      visitDate: [null],
      dischargeDate: [null],
      hospitalNo: [null],
      relatedGcl: [null],
      documentType: [null, Validators.required],
      remark: [null],

      insuredPaymentMethod: [null],
      payInName: [null]
    });

    this.claimModalService.relatedGcl.subscribe(res => {
      this.claimInfoForm.patchValue({
        relatedGcl: res
      });
    });


  }

  getClaimInfomation(){
    this.spinner.spinnerShow();
    this.faxclaimService.getInfomationForClaim(this.claimsInvoiceId)
      .pipe(
        finalize(() => this.spinner.spinnerHide())
      )
      .subscribe((res: InfomationForClaimDto) => {
        this.faxclaimService.infomationForClaimDto = res;
        console.log(this.faxclaimService);
        this.policyId = res.infomationClaims.policieId;

        for (let index = 0; index < res.infomationClaims.fileDocumentAttachments.length; index++) {
          this.spinner.spinnerShow();
          this.otherService.getFileDocumentAttachments(this.claimsInvoiceId, (res.infomationClaims.fileDocumentAttachments[index] as any).fileName)
            .pipe(
              finalize(() => this.spinner.spinnerHide())
            )
            .subscribe((_res) => {
              let files: Array<File> = [];
              let file = new File([_res], (res.infomationClaims.fileDocumentAttachments[index] as any).originalFileName, { type: _res.type });
              files.push(file);
              this.uploader.addToQueue(files);
            });
        }

        this.dataDetailInvoice.iaId = res.infomationPlanInsuringAgreements.planInsuringAgreementId;
        this.dataDetailInvoice.planId = res.infomationPlans.planId;
        this.dataDetailInvoice.hospitalId = res.infomationHospitals.hospitalId;

        this.getControlConfig(res, () => {
          const infomationClaims = (res.infomationClaims as any);
          this.selectedClaimTypeConcurrent = infomationClaims.claimTypeConcurrentProcessId;
          this.selectedDocumentType = infomationClaims.docType.id;
          this.selectedClaimTypeItems = [{ id: infomationClaims.claimsType.id, name: this.findLangForName(infomationClaims.claimsType.claimsTypesLangs).name }];

          if (infomationClaims.claimProcessId != null) {
            this.isProcess = true;
            this.selectedProcessItems = [{ id: infomationClaims.claimProcess.id, name: this.findLangForName(infomationClaims.claimProcess.claimProcessLangs).name }];
          }
          this.onClaimTypeItemItemSelect({ id: infomationClaims.claimsType.id });
          this.selectedInsuringAgreementItems = [{ id: infomationClaims.insuringAgreement.id, name: this.findLangForName(infomationClaims.insuringAgreement.insuringAgreementLangs).name }];
          this.selectedHospital = { id: infomationClaims.hospital.id, name: this.findLangForName(infomationClaims.hospital.hospitalsLangs).name };

          this.claimInfoForm.patchValue({
            invoiceDate: infomationClaims.invoiceDate,
            invoiceNo: infomationClaims.invoiceNo,
            receivedDate: infomationClaims.receivedDate,
            accidentDate: infomationClaims.accidentDate,
            admitDate: infomationClaims.admitDate,
            visitDate: infomationClaims.visitDate,
            dischargeDate: infomationClaims.dischargeDate,
            hospitalNo: infomationClaims.hospitalNo,
            relatedGcl: infomationClaims.claimsInvoiceRelatedGclId == null ? null : infomationClaims.claimsInvoiceRelatedGcl.gcl,
            remark: infomationClaims.remark
          });
        });

        this.spinner.spinnerShow();
        this.otherService.getConfigClaimPortalChangeStatus(
          {
            partnerId: this.faxclaimService.infomationForClaimDto.infomationPlans.plan.partnerId,
            planTypeId: this.faxclaimService.infomationForClaimDto.infomationPlans.plan.planTypeId,
            claimsTypeId: parseInt(this.faxclaimService.infomationForClaimDto.infomationClaims.claimsTypeId.toString()),
            statusId: parseInt(this.faxclaimService.infomationForClaimDto.infomationClaims.statusId.toString()),
            lang: {
              language: localStorage.getItem('lang'),
            }
          }
        )
          .pipe(
            finalize(() => this.spinner.spinnerHide())
          ).subscribe((res: ResponseConfigClaimPortalChangeStatusDto[]) => {
            this.faxclaimService.dataBtnChangeStatus = res;
            this.disabledTab = false;
          });
      });
  }

  btnRelatedGcl() {
    this.claimModalService.openModalRelatedGcl("Related GCL", this.route, { policyId: this.policyId }, this.modal);
  }

  getControlConfig(res: InfomationForClaimDto, callback?: Function) {
    //Get Claim Type
    this.portalService.getControlConfig(res.infomationPlans.planId)
      .subscribe((res: any) => {
        this.getClaimType = res;
        console.log(res);

        let claimTypeListItems = [];
        res.claimTyps.map(m => {
          claimTypeListItems.push({
            id: findLangForName(m.claimsTypesLangs).id,
            name: findLangForName(m.claimsTypesLangs).name
          });
        });
        this.claimTypeList = claimTypeListItems;

        let processListItem = [];
        res.claimProcessMaster.map(m => {
          processListItem.push({
            id: findLangForName(m.claimProcessLangs).id,
            name: findLangForName(m.claimProcessLangs).name
          });
        });
        this.processList = processListItem;

        callback();
      });
    //End Claim Type
  }

  onClaimTypeItemItemSelect(item: any) {
    this.isProcess = false;
    let insuringAgreements = [];
    this.selectedInsuringAgreementItems = [];
    let claimTyps = this.getClaimType.claimTyps.find(f => f.id == item.id);

    claimTyps.insuringAgreements.map(m => {
      insuringAgreements.push({
        id: findLangForName(m.insuringAgreementLangs).id,
        name: findLangForName(m.insuringAgreementLangs).name
      });
    });
    this.insuringAgreementList = insuringAgreements;

    claimTyps.subClaimTypes.map(m => {
      if (m.controlConfig.isProcess) {
        this.isProcess = true;
        return;
      }
    });
  }

  saveClaimFormSubmit() {

    if (this.claimInfoForm.valid) {
      this.alertify.confirm("Save Change", "คุณต้องการบันทึก ใช่หรือไม่", () => {
        const claimInfoForm = this.claimInfoForm.value;
        console.log(claimInfoForm);
        let claimReserveForBackendDto: ClaimReserveForBackendDto = {
          claimTypeConcurrent: claimInfoForm.claimTypeConcurrent,
          policy: <ObjectRefDto>{ id: this.policyId },
          hospital: <ObjectRefDto>{ id: claimInfoForm.hospital.id },
          insuringAgreement: <ObjectRefDto>{ id: claimInfoForm.insuringAgreement[0].id },
          claimType: <ObjectRefDto>{ id: claimInfoForm.claimType[0].id },
          accidentDate: claimInfoForm.accidentDate,
          invoiceDate: claimInfoForm.invoiceDate,
          visitDate: claimInfoForm.visitDate,
          admitDate: claimInfoForm.admitDate,
          dischargeDate: claimInfoForm.dischargeDate,
          receivedDate: claimInfoForm.receivedDate,
          invoiceNo: claimInfoForm.invoiceNo,
          hospitalNo: claimInfoForm.hospitalNo,
          relatedGcl: claimInfoForm.relatedGcl,
          documentType: <ObjectRefDto>{ id: claimInfoForm.documentType },
          remark: claimInfoForm.remark,
          expenses: 0,
          discount: 0,
          claimProcess: claimInfoForm.claimProcess == null ? claimInfoForm.claimProcess : <ObjectRefDto>{ id: claimInfoForm.claimProcess[0].id }
        };

        this.spinner.spinnerShow();
        if (!isNaN(this.claimsInvoiceId)) {
          let claimUpdateForBackendDto: ClaimUpdateForBackendDto = { ...claimReserveForBackendDto, claimsInvoice: <ObjectRefDto>{ id: this.claimsInvoiceId } };
          this.claimService.updateClaimForBackend(claimUpdateForBackendDto)
            .pipe(
              finalize(() => this.spinner.spinnerHide())
            ).subscribe((_res: any) => {
              this.router.navigate(['../../claim-info/' + _res.id], { relativeTo: this.route });
              this.getClaimInfomation();
            }, error => {
              this.toastrService.danger(error, "Error", { status: 'primary', position: NbGlobalLogicalPosition.BOTTOM_END });
            });
        } else {
          this.claimService.createClaimForBackend(claimReserveForBackendDto)
            .pipe(
              finalize(() => this.spinner.spinnerHide())
            ).subscribe((_res: any) => {
              this.router.navigate(['../../claim-info/' + _res.id], { relativeTo: this.route });
            }, error => {
              this.toastrService.danger(error, "Error", { status: 'primary', position: NbGlobalLogicalPosition.BOTTOM_END });
            });
        }
        this.isEdit = true;
      });
    } else {
      const errors = getFormValidationErrors(this.claimInfoForm);
      if (errors.length > 0) {
        errors.every(el => {
          if (el.isError) {
            this.toastrService.danger(el.field + ', ' + el.keyError, "Validators", { status: 'primary', position: NbGlobalLogicalPosition.BOTTOM_END });
            return false;
          }
        });
      }
    }

  }

  btnSaveClaimNote(){
    if (this.claimNote.trim() != ''){
      this.spinner.spinnerShow();
      this.faxclaimService.postClaimNote(this.claimsInvoiceId, this.claimNote)
      .pipe(
        finalize(() => this.spinner.spinnerHide())
      )
      .subscribe(res => {
        this.claimNote = '';
        (this.faxclaimService.infomationForClaimDto.infomationClaims as any).claimNoteLists.push(res);
      });
    }else{
      this.toastrService.warning("กรุณากรอกข้อมูลให้ครบ", "Warning", { status: 'primary', position: NbGlobalLogicalPosition.BOTTOM_END });
    }
  }

  onDeClaimTypeItemItemSelect(item: any) {
    this.selectedInsuringAgreementItems = [];
    this.insuringAgreementList = [];
    this.isProcess = false;
    this.selectedProcessItems = [];
  }

  btnUploadFileDocument() {
    this.backendUploadFileDto = [];
    this.uploader.uploadAll();
  }

  uploadFileDocument() {
    this.spinner.spinnerShow();
    this.otherService.postDocumentForCreateDocument(this.claimsInvoiceId, this.backendUploadFileDto)
      .pipe(
        finalize(() => this.spinner.spinnerHide())
      ).subscribe((_res: any[]) => {
        for (let index = 0; index < _res.length; index++) {
          this.otherService.getFileDocumentAttachments(this.claimsInvoiceId, _res[index].fileName)
            .subscribe((res) => {
              let files: Array<File> = [];
              let file = new File([res], _res[index].originalFileName, { type: res.type });
              files.push(file);
              this.uploader.addToQueue(files);
            });
        }
      });
  }

  viewFileUpload(item: any) {

    let saveByteArray = (function () {
      let a = document.createElement("a");
      document.body.appendChild(a);
      a.style.display = "none";
      return function (data, name) {
        var blob = new Blob(data, { type: "octet/stream" }),
          url = window.URL.createObjectURL(blob);
        a.href = url;
        a.download = name;
        a.click();
        window.URL.revokeObjectURL(url);
      };
    }());

    var _base64ToArrayBuffer = this;
    var reader = new FileReader();
    reader.readAsDataURL(item._file);
    reader.onloadend = function (e) {
      var sampleBytes = _base64ToArrayBuffer.base64ToArrayBuffer(reader.result.toString().split(',')[1]);
      saveByteArray([sampleBytes], item._file.name);
    }
  }

  base64ToArrayBuffer(base64) {
    var binaryString = window.atob(base64);
    var binaryLen = binaryString.length;
    var bytes = new Uint8Array(binaryLen);
    for (var i = 0; i < binaryLen; i++) {
      var ascii = binaryString.charCodeAt(i);
      bytes[i] = ascii;
    }
    return bytes;
  }

  statusReturn(statusClaim: string): string {
    if (statusClaim == "Pre-Authorized") {
      return "Pre-Authorized accepted";
    }
    else if (statusClaim == "Pre-Authorized accepted") {
      return "Concurrent";
    }
    else if (statusClaim == "Concurrent") {
      return "Discharge Request";
    }
    return "No Status";
  }

  saveChangeStatus(statusClaim: string) {
    // this.alertify.confirm("Save Change Status", "คุณต้องการบันทึก ใช่หรือไม่", () => {
    //   this.spinner.spinnerShow();
    //   this.faxclaimService.postInfomationForClaim(this.claimsInvoiceId)
    //   .pipe(
    //     finalize(() => this.spinner.spinnerHide())
    //   )
    //   .subscribe((res: InfomationClaim) => {
    //     this.infomationClaim = res;
    //     this.toastrService.success("Save Success!", "Save Change Status", {status: 'primary', position: NbGlobalLogicalPosition.BOTTOM_END})
    //   });
    // });
  }

  btnAdd() {
    this.modal.openModalConcurrentAddItem("Concurrent Add Item - " + this.faxclaimService.infomationForClaimDto.infomationClaims.gcl, this.route, { dashboardList: this.faxclaimService.infomationForClaimDto.infomationClaims }, null);
  }

  btnSearchConcurrent(concurrentSearch: string) {
    this.concurrentSearch = concurrentSearch;
  }

  btnPrintPdfConcurrentHistoryPdfFile() {
    this.spinner.spinnerShow();
    this.faxclaimService.getConcurrentHistoryPdfFile(this.claimsInvoiceId)
      .pipe(
        finalize(() => this.spinner.spinnerHide())
      ).subscribe((_res) => {
        const blob = new Blob([_res], { type: _res.type });
        var a = document.createElement("a");
        a.href = URL.createObjectURL(blob);
        a.download = 'Concurrent_' + moment().format('YYYY_MM_DD') + '_' + this.faxclaimService.infomationForClaimDto.infomationClaims.gcl + '.pdf';
        a.click();
      });
  }

  treatmentLang(treatment: Treatment): string {
    return treatment.treatmentLangs.find(f => f.language == localStorage.getItem('lang')).name;
  }

  healthDiagnosisLang(healthDiagnosis: HealthDiagnosis): string {
    return healthDiagnosis.healthDiagnosisLangs.find(f => f.language == localStorage.getItem('lang')).name;
  }

  private loadDisease10Item() {
    this.diseaseItem10$ = concat(
      of([]),
      this.diseaseInput10$.pipe(
        debounceTime(200),
        distinctUntilChanged(),
        tap(() => this.diseaseLoading10 = true),
        switchMap(term => this.otherService.getDisease(term, 10)
          .pipe(
            catchError(() => of([])),
            tap(() => this.diseaseLoading10 = false)
          ))
      )
    );
  }

  private loadHospital() {
    this.hospitalItem$ = concat(
      of([]),
      this.hospitalInput$.pipe(
        debounceTime(200),
        distinctUntilChanged(),
        tap(() => this.hospitalLoading = true),
        switchMap(term => this.otherService.getHospital(term)
          .pipe(
            catchError(() => of([])),
            tap(() => this.hospitalLoading = false)
          ))
      )
    );
  }

  private loadDisease9Item() {
    this.diseaseItem9$ = concat(
      of([]),
      this.diseaseInput9$.pipe(
        debounceTime(200),
        distinctUntilChanged(),
        tap(() => this.diseaseLoading9 = true),
        switchMap(term => this.otherService.getDisease(term, 9)
          .pipe(
            catchError(() => of([])),
            tap(() => this.diseaseLoading9 = false)
          ))
      )
    );
  }

  trackByFn(item: DiseaseItem) {
    return item.id;
  }

  addDiagnosis10(selectedDiseases10: any) {
    console.log(selectedDiseases10);
    var diseases10CheckDub = this.diseases10.find(f => {
      if (f.id == selectedDiseases10.id) {
        this.toastrService.danger("ICD10 Dubpicate List", "Error", { status: 'primary', position: NbGlobalLogicalPosition.BOTTOM_END });
        return f;
      }
    });

    if (typeof diseases10CheckDub == 'undefined') {
      selectedDiseases10["used"] = true;
      selectedDiseases10["primary"] = true;
      this.diseases10.map(m => m.primary = false);
      this.diseases10.push(selectedDiseases10);

      this.selectedDiseases10 = [];
    }
  }

  deleteDiagnosis10(index: number) {
    this.alertify.confirm("Delete Disease10", "คุณต้องการลบ ใช่หรือไม่", () => {
      this.diseases10.splice(index, 1);
    });
  }

  checkedDiseases10(event: boolean, item: any) {
    item.used = event;
  }

  modelChangedDiseases10Primary(event: any, item: any) {
    this.diseases10.map(m => m.primary = false);
    item.primary = true;
  }

  addDiagnosis9(selectedDiseases9: any) {
    var diseases9CheckDub = this.diseases9.find(f => {
      if (f.id == selectedDiseases9.id) {
        this.toastrService.danger("ICD9 Dubpicate List", "Error", { status: 'primary', position: NbGlobalLogicalPosition.BOTTOM_END });
        return f;
      }
    });

    if (typeof diseases9CheckDub == 'undefined') {
      selectedDiseases9["used"] = true;
      selectedDiseases9["primary"] = true;
      this.diseases9.map(m => m.primary = false);
      this.diseases9.push(selectedDiseases9);

      this.selectedDiseases9 = [];
    }
  }

  deleteDiagnosis9(index: number) {
    this.alertify.confirm("Delete Disease9", "คุณต้องการลบ ใช่หรือไม่", () => {
      this.diseases9.splice(index, 1);
    });
  }

  checkedDiseases9(event: boolean, item: any) {
    item.used = event;
  }

  modelChangedDiseases9Primary(event: any, item: any) {
    this.diseases9.map(m => m.primary = false);
    item.primary = true;
  }

  saveDisease() {
    let valueArr = [
      { value: this.selectHealthDiagnosis == null, message: "HealthDiagnosis, require" },
      { value: this.selectTreatments == null, message: "Treatments, require" },
      { value: this.diseases9.length <= 0 && this.diseases10.length <= 0, message: "กรุณาเลือก ICD10 หรือ ICD9" },
    ];
    valueArr.every(el => {
      if (el.value) {
        this.toastrService.danger(el.message, "Validators", { status: 'primary', position: NbGlobalLogicalPosition.BOTTOM_END });
        return false;
      }
      return true;
    });

    if (valueArr.findIndex(f => f.value == true) >= 0) { return; }
    else {
      this.alertify.confirm("Save Disease", "คุณต้องการบันทึก ใช่หรือไม่", () => {
        this.claimsInvoiceDiagnosisedDto.diseaseICD9 = [];
        this.diseases9.map(m => {
          var diseaseDto: DiseaseDto = <DiseaseDto>{};
          diseaseDto.id = m.id;
          diseaseDto.used = m.used;
          diseaseDto.primary = m.primary;
          this.claimsInvoiceDiagnosisedDto.diseaseICD9.push(diseaseDto);
        });

        this.claimsInvoiceDiagnosisedDto.diseaseICD10 = [];
        this.diseases10.map(m => {
          var diseaseDto: DiseaseDto = <DiseaseDto>{};
          diseaseDto.id = m.id;
          diseaseDto.used = m.used;
          diseaseDto.primary = m.primary;
          this.claimsInvoiceDiagnosisedDto.diseaseICD10.push(diseaseDto);
        });

        this.claimsInvoiceDiagnosisedDto.claimsInvoiceId = this.claimsInvoiceId;
        this.claimsInvoiceDiagnosisedDto.healthDiagnosisId = this.selectHealthDiagnosis;
        this.claimsInvoiceDiagnosisedDto.treatmentId = this.selectTreatments;

        this.spinner.spinnerShow();
        this.faxclaimService.postClaimsInvoiceDiagnosised(this.claimsInvoiceDiagnosisedDto)
          .pipe(
            finalize(() => this.spinner.spinnerHide())
          ).subscribe((res: any) => {
            this.diseases9 = res.diseaseICD9;
            this.diseases10 = res.diseaseICD10;
            this.selectHealthDiagnosis = res.healthDiagnosisId;
            this.selectTreatments = res.treatmentId;

            this.toastrService.success("Save Success!", "Success", { status: 'primary', position: NbGlobalLogicalPosition.BOTTOM_END });
          }, error => {
            this.toastrService.danger(error, "Error", { status: 'primary', position: NbGlobalLogicalPosition.BOTTOM_END });
          });
      });
    }
  }

}
