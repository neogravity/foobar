import { Component, OnInit, Input, ViewChild, ElementRef, Renderer, Renderer2 } from '@angular/core';
import { ExpensesDataDto, ex, be } from '../../../../../DtoView/claim-info/expenses-data-dto';
import { DataService } from '../../../../FAX-CLAIM/data.service';
import { SpinnerService } from '../../../../../_services/spinner.service';
import { AlertifyService } from '../../../../../_services/alertify.service';
import { NbToastrService, NbGlobalLogicalPosition } from '@nebular/theme';
import { Observable, Subject, of, concat } from 'rxjs';
import { debounceTime, distinctUntilChanged, tap, switchMap, catchError, finalize } from 'rxjs/operators';
import { OtherService } from '../../../../../_services/other.service';
import { ResponseExpenseForExpenseItem } from '../../../../../DtoView/claim-info/ResponseExpenseForExpenseItem';
import { NgSelectConfig } from '@ng-select/ng-select';
import { Router, ActivatedRoute } from '@angular/router';
import { IaDto } from '../../../../../DtoView/claim-info/ExpenseGroup/IaDto';
import { forceCast, forceCastArray } from '../../../../../_helpers/forceCast';

@Component({
  selector: 'ngx-expensess',
  templateUrl: './expensess.component.html',
  styleUrls: ['./expensess.component.scss']
})
export class ExpensessComponent implements OnInit {
  @Input('expensessData') expensessData: ExpensesDataDto;
  @Input('dataDetailInvoice') dataDetailInvoice: any;

  expenseItem$: Observable<ResponseExpenseForExpenseItem[]>;
  expenseLoading = false;
  expenseInput$ = new Subject<string>();
  selectedExpense: ResponseExpenseForExpenseItem[];
  expense: any[] = <any>[];
  selectExpensePrimary: any;
  viewExpense: boolean = false;
  searchDataEntry: string;

  constructor(public data: DataService,private config: NgSelectConfig, private spinner: SpinnerService, private alertify: AlertifyService, private toastrService: NbToastrService, public otherService: OtherService, private renderer: Renderer2, private router: Router, private route: ActivatedRoute) { 
    this.config.notFoundText = 'Expense not found';
  }

  ngOnInit() {
    this.loadExpenseItem();
  }

  deleteEx(co: any, be: any, ex: any, index: number){
    // console.log(ia.id);
    // console.log(co.id);
    // console.log(be.id);
    // console.log(ex.id);

    // this.alertify.confirm("Remove Expense Item", "คุณต้องการลบ ใช่หรือไม่", () => {
    //   const exItem = this.data.expensessData.ia.co.find(f => f.id == co.id).be.find(f => f.id == be.id).ex;
    //   if(exItem.length > 1){
    //     exItem.splice(index, 1);
    //   }else{
    //     this.toastrService.danger("ไม่สามารถลบได้ Expense Item ไม่ควรน้อยกว่า 1 Item", "Error", { status: 'primary', position: NbGlobalLogicalPosition.BOTTOM_END });
    //   }
    // });

    this.alertify.confirm("Remove Expense Item", "คุณต้องการลบ ใช่หรือไม่", () => {
      const exItem = this.expensessData.ia.co.find(f => f.id == co.id).be.find(f => f.id == be.id).ex;
      exItem.splice(index, 1);

      this.resetExpense();
    });

  }

  changeValue(event: any, co: any, be: any, ex: any, eventReject: boolean = false){
    // console.log(event);
    // console.log(event);
    // console.log(co);
    // console.log(be);
    // console.log(ex);

    var exData = this.expensessData.ia
      .co.find(f => f.id == co.id)
      .be.find(f => f.id == be.id)
      .ex.find(f => f.id == ex.id);

    if (event.target.name == 'expensesTotal'){
      exData.reject = 0;
      if (eventReject){
        exData.reject = exData.unit <= 0 ? 0 : (parseFloat(exData.expensesTotal.toString()) + parseFloat(exData.reject.toString())) * exData.unit;
      }
      exData.expensesTotal = event.target.value;
      exData.defaultExpensesTotal = event.target.value;
      if (exData.expensesTotal == 0){
        exData.discount = 0;
        exData.discountMod = exData.hpDiscount;
      }
    }
    else if (event.target.name == 'reject'){
      if (exData.expensesTotal > 0)
      {
        exData.reject = event.target.value;
        exData.isReject = false;
      }else{
        this.toastrService.danger("Expense ต้องมากกว่า 0", "Error", { status: 'primary', position: NbGlobalLogicalPosition.BOTTOM_END });
        return;
      }
    }
    else if (event.target.name == 'unit'){
      if (event.target.value <= 0 || event.target.value == null || event.target.value == '') 
      {
        this.toastrService.danger("Unit ต้องมากกว่า 0", "Error", { status: 'primary', position: NbGlobalLogicalPosition.BOTTOM_END });
        return;
      }
      exData.unit = event.target.value <= 0 || event.target.value == null || event.target.value == '' ? 1 : event.target.value;
    }
    else if (event.target.name == 'discount'){
      exData.discount = event.target.value;
      exData.discountMod = exData.discount <= 0 ? 0 : parseFloat(((exData.discount / exData.expensesTotal) * 100).toFixed(2));
    }

    if (exData.discountMod > 0 && exData.expensesTotal > 0 && event.target.name == 'expensesTotal'){
      exData.discount = (exData.expensesTotal * exData.discountMod) / 100;
      exData.discountMod = exData.discount <= 0 ? 0 : parseFloat(((exData.discount / exData.expensesTotal) * 100).toFixed(2));
    }

    exData.expensesUnit = parseFloat((exData.expensesTotal / exData.unit).toFixed(2));
    exData.total = parseFloat((exData.expensesTotal - exData.discount).toFixed(2));

    this.resetExpense();
  }

  rejectPartial(co: any, be: any, ex: any){
    var event = { target: { name: "expensesTotal", value: 0 } };
    this.expensessData.ia
      .co.find(f => f.id == co.id)
      .be.find(f => f.id == be.id)
      .ex.find(f => f.id == ex.id).isReject = true;
    this.changeValue(event, co, be, ex, true);
  }

  rejectItemEnter(co: any, be: any, ex: any){
    var exData = this.expensessData.ia
      .co.find(f => f.id == co.id)
      .be.find(f => f.id == be.id)
      .ex.find(f => f.id == ex.id);
      if (parseFloat(exData.reject.toString()) > parseFloat(exData.expensesTotal.toString())){
        this.toastrService.danger("Reject ต้องน้อยกว่า Expense", "Error", { status: 'primary', position: NbGlobalLogicalPosition.BOTTOM_END });
        return;
      }
    exData.expensesTotal = exData.reject > 0 ? (exData.defaultExpensesTotal - exData.reject) : exData.expensesTotal;
    var event = { target: { name: "reject", value: exData.reject } };
    this.changeValue(event, co, be, ex, true);
  }

  rejectItem(co: any, be: any, ex: any){
    var exData = this.expensessData.ia
      .co.find(f => f.id == co.id)
      .be.find(f => f.id == be.id)
      .ex.find(f => f.id == ex.id);

    if (exData.expensesTotal > 0)
    {
      exData.isReject = false;
    }else{
      this.toastrService.danger("Expense ต้องมากกว่า 0", "Error", { status: 'primary', position: NbGlobalLogicalPosition.BOTTOM_END });
    }
  }

  resetExpense(){
    this.expensessData.ia.co.forEach(co => {
      co.be.forEach(be => {
        be.expense = 0;
        be.discount = 0;
        be.total = 0;
        be.ex.forEach(ex => {
          be.expense += parseFloat(ex.expensesTotal.toString());
          be.discount += parseFloat(ex.discount.toString());
          be.total += ex.total;
        });
      });
    });

    this.expensessData.ia.co.forEach(co => {
      co.expense = 0;
      co.discount = 0;
      co.total = 0;
      co.be.forEach(be => {
        co.expense += be.expense;
        co.discount += be.discount;
        co.total += be.total;
      });
    });

    this.expensessData.ia.expense = 0;
    this.expensessData.ia.discount = 0;
    this.expensessData.ia.total = 0;
    this.expensessData.ia.co.forEach(co => {
      this.expensessData.ia.expense += co.expense;
      this.expensessData.ia.discount += co.discount;
      this.expensessData.ia.total += co.total;
    });
  }

  onChangeExpense(selectedExpense: any){
    this.addExpenseItemDataEntry(selectedExpense);
  }

  addExpenseItemDataEntry(selectedExpense: any){
    this.spinner.spinnerShow();
    this.otherService.getExpenseItemById(this.dataDetailInvoice.planId, this.dataDetailInvoice.iaId, selectedExpense.beId, selectedExpense.exId, this.dataDetailInvoice.hospitalId)
    .pipe(
      finalize(() => this.spinner.spinnerHide())
    ).subscribe((res: IaDto[]) => {
      if (this.expensessData.ia == null){
        this.expensessData.ia.expense = 0;
        this.expensessData.ia.discount = 0;
        this.expensessData.ia.total = 0;
        this.expensessData.ia.cover = 0;
        this.expensessData.ia.uncover = 0;
        this.expensessData.ia.deductible = 0;
        this.expensessData.ia.coPayment = 0;
      }
      this.expensessData.ia.id = res[0].id;
      this.expensessData.ia.itemName = res[0].insuringAgreementLangs.find(f => f.language == localStorage.getItem('lang')).name;
      this.expensessData.ia.description = res[0].benefitDescription;
      this.expensessData.ia.isCollapsed = true;

      res.map(ia => {
        ia.coDtos.map(co => {
          if (this.expensessData.ia.co == null)
          {
            this.expensessData.ia.co = <any>[];
          }
          if (typeof this.expensessData.ia.co.find(f => f.id == co.id) == 'undefined')
          {
            this.expensessData.ia.co.push({
              id: co.id,
              itemName: co.coveragesLangs.find(f => f.language == localStorage.getItem('lang')).name,
              expense: 0,
              discount: 0,
              total: 0,
              cover: 0,
              uncover: 0,
              deductible: 0,
              coPayment: 0,
              description: co.benefitDescription,
              isCollapsed: true,
              be: []
            });
          }

          co.beDtos.map(be => {
            if (this.expensessData.ia.co.find(f => f.id == co.id).be == null)
            {
              this.expensessData.ia.co.find(f => f.id == co.id).be = <any>[];
            }
            if (typeof this.expensessData.ia.co.find(f => f.id == co.id).be.find(f => f.id == be.id) == 'undefined')
            {
              this.expensessData.ia.co.find(f => f.id == co.id).be.push({
                id: be.id,
                itemName: be.benefitLangs.find(f => f.language == localStorage.getItem('lang')).name,
                expense: 0,
                discount: 0,
                total: 0,
                cover: 0,
                uncover: 0,
                deductible: 0,
                coPayment: 0,
                description: be.benefitDescription,
                isCollapsed: true,
                ex: []
              });
            }

            be.exDtos.map(ex => {
              if (this.expensessData.ia.co.find(f => f.id == co.id).be.find(f => f.id == be.id).ex == null)
              {
                this.expensessData.ia.co.find(f => f.id == co.id).be.find(f => f.id == be.id).ex = <any>[];
              }
              if (typeof this.expensessData.ia.co.find(f => f.id == co.id).be.find(f => f.id == be.id).ex.find(f => f.id == ex.id) == 'undefined')
              {
                this.expensessData.ia.co.find(f => f.id == co.id).be.find(f => f.id == be.id).ex.push({
                  id: ex.id,
                  itemName: ex.expensesLangs.find(f => f.language == localStorage.getItem('lang')).name,
                  expensesUnit: 0,
                  unit: 1,
                  discountMod: ex.discount,
                  expensesTotal: 0,
                  defaultExpensesTotal: 0,
                  discount: 0,
                  hpDiscount: ex.discount,
                  reject: 0,
                  isReject: true,
                  total: 0
                })
              }else{
                this.toastrService.danger("Expense Item Dupicate", "Error", { status: 'primary', position: NbGlobalLogicalPosition.BOTTOM_END });
              }
            })
          })
        });
      });
    });
    this.selectedExpense = [];
  }

  private loadExpenseItem() {
    this.expenseItem$ = concat(
      of([]),
      this.expenseInput$.pipe(
        debounceTime(200),
        distinctUntilChanged(),
        tap(() => this.expenseLoading = true),
        switchMap(term => this.otherService.getExpenseItemBySearch(term, this.dataDetailInvoice.planId, this.dataDetailInvoice.iaId, this.dataDetailInvoice.hospitalId)
          .pipe(
            catchError(() => of([])),
            tap(() => this.expenseLoading = false)
          ))
      )
    );
  }

  trackByFn(item: ResponseExpenseForExpenseItem) {
    return item.exId;
  }

}
