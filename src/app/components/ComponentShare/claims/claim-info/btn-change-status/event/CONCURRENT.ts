import { FaxclaimService } from '../../../../../../_services/faxclaim.service';
import { SpinnerService } from '../../../../../../_services/spinner.service';
import { AlertifyService } from '../../../../../../_services/alertify.service';
import { NbToastrService, NbGlobalLogicalPosition } from '@nebular/theme';
import { InfomationClaim, InfomationForClaimDto } from '../../../../../../DtoView/claim-info/Infomation-claim';
import { finalize } from 'rxjs/operators';
import { RequestPostInfomationForClaimDto } from '../../../../../../DtoView/claim-info/RequestPostInfomationForClaimDto';
import { OtherService } from '../../../../../../_services/other.service';
import { ResponseConfigClaimPortalChangeStatusDto } from '../../../../../../DtoView/claim-info/ResponseConfigClaimPortalChangeStatusDto';
import { ClaimUpdateForBackendDto } from '../../../../../../DtoView/create-claim/ClaimUpdateForBackendDto';

export class CONCURRENT {
    constructor(private otherService: OtherService, private faxclaimService: FaxclaimService, private spinner: SpinnerService, private alertify: AlertifyService, private toastrService: NbToastrService) { }

    saveChangeStatus(infomationClaim: InfomationClaim, claimUpdateForBackendDtoReq: ClaimUpdateForBackendDto, dataBtnChangeStatus: any) {
        this.alertify.confirm("Save Change Status", "คุณต้องการบันทึก ใช่หรือไม่", () => {
            const requestPostInfomationForClaimDto: RequestPostInfomationForClaimDto = {
                claimsInvoiceId: parseInt(infomationClaim.id.toString()),
                statusId: dataBtnChangeStatus.statusId,
                claimUpdateForBackendDto: claimUpdateForBackendDtoReq
            };
            this.spinner.spinnerShow();
            this.faxclaimService.postInfomationForClaim(requestPostInfomationForClaimDto)
                .pipe(
                    finalize(() => this.spinner.spinnerHide())
                )
                .subscribe((res: InfomationForClaimDto) => {
                    this.faxclaimService.infomationForClaimDto.infomationClaims = res.infomationClaims;

                    this.spinner.spinnerShow();
                    this.otherService.getConfigClaimPortalChangeStatus(
                        {
                            partnerId: this.faxclaimService.infomationForClaimDto.infomationPlans.plan.partnerId,
                            planTypeId: this.faxclaimService.infomationForClaimDto.infomationPlans.plan.planTypeId,
                            claimsTypeId: parseInt(this.faxclaimService.infomationForClaimDto.infomationClaims.claimsTypeId.toString()),
                            statusId: parseInt(this.faxclaimService.infomationForClaimDto.infomationClaims.statusId.toString()),
                            lang: {
                                language: localStorage.getItem('lang'),
                            }
                        }
                    )
                        .pipe(
                            finalize(() => this.spinner.spinnerHide())
                        ).subscribe((res: ResponseConfigClaimPortalChangeStatusDto[]) => {
                            this.faxclaimService.dataBtnChangeStatus = res;
                        });
                    this.toastrService.success("Save Success!", "Save Change Status", { status: 'primary', position: NbGlobalLogicalPosition.BOTTOM_END })
                });
        });
    }
}
