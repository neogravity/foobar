import { Component, OnInit, Input } from '@angular/core';
import { FaxclaimService } from '../../../../../_services/faxclaim.service';
import { AlertifyService } from '../../../../../_services/alertify.service';
import { SpinnerService } from '../../../../../_services/spinner.service';
import { NbToastrService, NbGlobalLogicalPosition } from '@nebular/theme';
import { InfomationClaim } from '../../../../../DtoView/claim-info/Infomation-claim';
import { WAIT_FOR_ASSESSMENT } from './event/WAIT_FOR_ASSESSMENT';
import { PRE_AUTH_ACCEPT } from './event/PRE_AUTH_ACCEPT';
import { CONCURRENT } from './event/CONCURRENT';
import { DISCHARGFE_REQUEST } from './event/DISCHARGFE_REQUEST';
import { OtherService } from '../../../../../_services/other.service';
import { FormGroup } from '@angular/forms';
import { getFormValidationErrors } from '../../../../../_helpers/getFormValidationErrors';
import { ClaimReserveForBackendDto, ObjectRefDto } from '../../../../../DtoView/create-claim/ClaimReserveForBackendDto';
import { ClaimUpdateForBackendDto } from '../../../../../DtoView/create-claim/ClaimUpdateForBackendDto';
import { ClaimService } from '../../../../../_services/claim.service';

@Component({
  selector: 'ngx-btn-change-status',
  templateUrl: './btn-change-status.component.html',
  styleUrls: ['./btn-change-status.component.scss']
})
export class BtnChangeStatusComponent implements OnInit {
  @Input('dataBtnChangeStatus') dataBtnChangeStatus: any;
  @Input('infomationClaim') infomationClaim: InfomationClaim;
  @Input('claimInfoForm') claimInfoForm: FormGroup;
  claimUpdateForBackendDtoReq: ClaimUpdateForBackendDto = <any>{};

  constructor(public claimService: ClaimService, public otherService: OtherService, public faxclaimService: FaxclaimService, private spinner: SpinnerService, private alertify: AlertifyService, private toastrService: NbToastrService) { }

  wait_for_assessment: WAIT_FOR_ASSESSMENT = new WAIT_FOR_ASSESSMENT(this.otherService, this.faxclaimService, this.spinner, this.alertify, this.toastrService);
  pre_authorized_accepted: PRE_AUTH_ACCEPT = new PRE_AUTH_ACCEPT(this.otherService, this.faxclaimService, this.spinner, this.alertify, this.toastrService);
  concurrent: CONCURRENT = new CONCURRENT(this.otherService, this.faxclaimService, this.spinner, this.alertify, this.toastrService);
  discharge_request: DISCHARGFE_REQUEST = new DISCHARGFE_REQUEST(this.otherService, this.faxclaimService, this.spinner, this.alertify, this.toastrService);

  ngOnInit() {
  }

  loadClaimInfoForm(calback: Function)
  {
    const claimInfoForm = this.claimInfoForm.value;
    let claimReserveForBackendDto: ClaimReserveForBackendDto = {
      claimTypeConcurrent: claimInfoForm.claimTypeConcurrent,
      policy: <ObjectRefDto>{ id: this.infomationClaim.policieId },
      hospital: <ObjectRefDto>{ id: claimInfoForm.hospital.id },
      insuringAgreement: <ObjectRefDto>{ id: claimInfoForm.insuringAgreement[0].id },
      claimType: <ObjectRefDto>{ id: claimInfoForm.claimType[0].id },
      accidentDate: claimInfoForm.accidentDate,
      invoiceDate: claimInfoForm.invoiceDate,
      visitDate: claimInfoForm.visitDate,
      admitDate: claimInfoForm.admitDate,
      dischargeDate: claimInfoForm.dischargeDate,
      receivedDate: claimInfoForm.receivedDate,
      invoiceNo: claimInfoForm.invoiceNo,
      hospitalNo: claimInfoForm.hospitalNo,
      relatedGcl: claimInfoForm.relatedGcl,
      documentType: <ObjectRefDto>{ id: claimInfoForm.documentType },
      remark: claimInfoForm.remark,
      expenses: 0,
      discount: 0,
      claimProcess: claimInfoForm.claimProcess == null ? claimInfoForm.claimProcess : <ObjectRefDto>{ id: claimInfoForm.claimProcess[0].id }
    };

    let claimUpdateForBackendDto: ClaimUpdateForBackendDto = { ...claimReserveForBackendDto, claimsInvoice: <ObjectRefDto>{ id: this.infomationClaim.id } };
    this.claimUpdateForBackendDtoReq = claimUpdateForBackendDto;

    calback();
  }

  changeStatus(){
    this.loadClaimInfoForm(cal => {
      if (this.claimInfoForm.valid){
        switch (this.dataBtnChangeStatus.statusCode) {
          case "WAIT_FOR_ASSESSMENT":
            this.wait_for_assessment.saveChangeStatus(this.infomationClaim, this.claimUpdateForBackendDtoReq, this.dataBtnChangeStatus);
            break;
          case "PRE_AUTH_ACCEPT":
            this.pre_authorized_accepted.saveChangeStatus(this.infomationClaim, this.claimUpdateForBackendDtoReq, this.dataBtnChangeStatus);
            break;
          case "CONCURRENT":
            this.concurrent.saveChangeStatus(this.infomationClaim, this.claimUpdateForBackendDtoReq, this.dataBtnChangeStatus);
            break;
          case "DISCHARGE_REQUEST":
            this.discharge_request.saveChangeStatus(this.infomationClaim, this.claimUpdateForBackendDtoReq, this.dataBtnChangeStatus);
            break;
          default:
            break;
        }
      }else{
        const errors = getFormValidationErrors(this.claimInfoForm);
        if (errors.length > 0)
        {
          errors.every(el => {
            if (el.isError){
              this.toastrService.danger(el.field + ', ' + el.keyError, "Validators", { status: 'primary', position: NbGlobalLogicalPosition.BOTTOM_END });
              return false;
            }
          });
        }
      }
    });
    
  }
}
