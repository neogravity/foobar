import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertifyService } from '../../../../_services/alertify.service';
import { SpinnerService } from '../../../../_services/spinner.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { PolicyService } from '../../../../_services/policy.service';
import { NbToastrService, NbGlobalLogicalPosition } from '@nebular/theme';
import { Pagination, PaginatedResult } from '../../../../_models/pagination';
import * as moment from 'moment';
import { ClaimService } from '../../../../_services/claim.service';
import { RequestListCriteriaDto } from '../../../../DtoView/BaseDto/RequestListCriteriaDto';
import { ClaimListFilterDto } from '../../../../DtoView/claim-list/ClaimListFilterDto';
import { finalize } from 'rxjs/operators';
import { ClaimsInvoice } from '../../../../_models/ClaimsInvoice';
import { findLangForName } from '../../../../_helpers/findLangForName';
import { checkNullOrEmtry } from '../../../../_helpers/checkNullOrEmtry';
import { BsDatepickerConfig } from 'ngx-bootstrap';
import { calPage } from '../../../../_helpers/calPage';

@Component({
  selector: 'ngx-claim-list',
  templateUrl: './claim-list.component.html',
  styleUrls: ['./claim-list.component.scss']
})
export class ClaimListComponent implements OnInit {
  currentPage = 1;
  page: number;
  totalItems: number;
  pagination: Pagination;
  claims: ClaimsInvoice[] = [];
  moment: Function = moment;
  calPage: Function = calPage;
  claimFormSearch: FormGroup;

  findLangForName: Function = findLangForName;

  bsConfig: Partial<BsDatepickerConfig>;
  colorTheme = 'theme-dark-blue';

  constructor(private claimService: ClaimService, private toastrService: NbToastrService, private policyService: PolicyService, private fb: FormBuilder, private spinner: SpinnerService, private alertify: AlertifyService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.claimFormSearch = this.fb.group({
      insuredPersonName: [null],
      insuredPersonSurname: [null],
      policyNo: [null],
      memberNo: [null],
      gcl: [null],
      caseNo: [null],
      statusId: [null],
      createdDateFrom: [null],
      createdDateTo: [null],
    });

    this.bsConfig = Object.assign({}, {
      containerClass: this.colorTheme,
      dateInputFormat: 'DD-MM-YYYY'
    });
  }

  btnSearchClaim(){
    const { insuredPersonName, insuredPersonSurname, memberNo, policyNo, gcl, caseNo, statusId, createdDateFrom, createdDateTo } = this.claimFormSearch.value;

    if (checkNullOrEmtry(insuredPersonName)
    && checkNullOrEmtry(insuredPersonSurname)
    && checkNullOrEmtry(memberNo)
    && checkNullOrEmtry(gcl)
    && checkNullOrEmtry(caseNo)
    && checkNullOrEmtry(statusId)
    && checkNullOrEmtry(createdDateFrom)
    && checkNullOrEmtry(createdDateTo)
    && checkNullOrEmtry(policyNo)){
      this.toastrService.warning("กรุณากรอกข้อมูลให้ครบ", "Warning", { status: 'primary', position: NbGlobalLogicalPosition.BOTTOM_END })
      return true;
    }

    this.searchClaim(this.claimFormSearch.value, 1);
  }

  searchClaim({...params}, page: number){
    const { insuredPersonName, insuredPersonSurname, memberNo, policyNo, gcl, caseNo, statusId, createdDateFrom, createdDateTo } = params;

    const requestListCriteriaDto: RequestListCriteriaDto<ClaimListFilterDto> = new RequestListCriteriaDto<ClaimListFilterDto>();

    requestListCriteriaDto.filter.firstname = insuredPersonName;
    requestListCriteriaDto.filter.lastname = insuredPersonSurname;
    requestListCriteriaDto.filter.memberNo = memberNo;
    requestListCriteriaDto.filter.policyNo = policyNo;
    requestListCriteriaDto.filter.gcl = gcl;
    requestListCriteriaDto.filter.caseNo = caseNo;
    requestListCriteriaDto.filter.statusId = statusId;
    requestListCriteriaDto.filter.createdDateFrom = createdDateFrom;
    requestListCriteriaDto.filter.createdDateTo = createdDateTo;
    

    this.spinner.spinnerShow();
    this.claimService.claimsSearch(page, requestListCriteriaDto)
    .pipe(
      finalize(() => this.spinner.spinnerHide())
    )
    .subscribe((res: PaginatedResult<ClaimsInvoice[]>) => {
      this.claims = res.result;
      console.log(this.claims);
      this.pagination = res.pagination;
      this.totalItems = res.pagination.totalItems;
    }, error => {
      this.alertify.error(error);
    });
  }


  pageChanged(event: any): void {
    this.page = event.page;
    this.searchClaim(this.claimFormSearch.value, this.page);
  }

  resetForm(){
    this.claimFormSearch.reset();
  }

  viewGcl(item: ClaimsInvoice){
    this.router.navigate(['../claim-info/' + item.id], { relativeTo: this.route });
  }

}
