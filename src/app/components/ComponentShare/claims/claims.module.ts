import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClaimsComponent } from './claims.component';
import { ClaimsRoutingModule } from './claims-routing.module';
import { NbIconModule, NbTabsetModule, NbRadioModule, NbAccordionModule, NbCheckboxModule } from '@nebular/theme';
import { ClaimListComponent } from './claim-list/claim-list.component';
import { ClaimInfoComponent } from './claim-info/claim-info.component';
import { SharedModule } from '../../../shared/shared.module';
import { FormsModule } from '@angular/forms';
import { ModalModule, BsDatepickerModule, CollapseModule } from 'ngx-bootstrap';
import { FileUploadModule } from 'ng2-file-upload';
import { ExpensessComponent } from './claim-info/expensess/expensess.component';
import { DirectiveModule } from '../../../shared/directives.module';
import { BtnChangeStatusComponent } from './claim-info/btn-change-status/btn-change-status.component';
import { CreateClaimComponent } from './create-claim/create-claim.component';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { ClaimTypeConcurrentProcessResolver } from '../../../_resolvers/disease-resolvers';
import { DoctypeResolver } from '../../../_resolvers/doctype-resolvers';
import { RelatedGclComponent } from './claim-info/modal/related-gcl/related-gcl.component';
import { ClaimModalService } from './claim-modal.service';

@NgModule({
  imports: [
    CommonModule,
    ClaimsRoutingModule,
    FormsModule,
    NbIconModule,
    NbTabsetModule,
    NbRadioModule,
    NbCheckboxModule,
    SharedModule,
    DirectiveModule,
    CollapseModule.forRoot(),
    PaginationModule.forRoot(),
    NgMultiSelectDropDownModule.forRoot(),

    ModalModule.forRoot(),
    NbAccordionModule,
    BsDatepickerModule.forRoot(),
    FileUploadModule
  ],
  declarations: [
    ClaimsComponent,
    ClaimListComponent,
    ClaimInfoComponent,

    ExpensessComponent,
    BtnChangeStatusComponent,
    CreateClaimComponent,
    RelatedGclComponent
  ],
  entryComponents: [
    RelatedGclComponent
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
  providers: [
    ClaimTypeConcurrentProcessResolver,
    DoctypeResolver,
    ClaimModalService
  ]
})
export class ClaimsModule { }
