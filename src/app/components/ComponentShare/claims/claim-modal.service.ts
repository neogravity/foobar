import { Injectable } from '@angular/core';
import { BsModalRef, BsModalService, ModalOptions } from 'ngx-bootstrap';
import { ActivatedRoute } from '@angular/router';
import { ModalService } from '../../FAX-CLAIM/modal.service';
import { RelatedGclComponent } from './claim-info/modal/related-gcl/related-gcl.component';
import { Subject } from 'rxjs/Subject';

@Injectable({
  providedIn: 'root'
})
export class ClaimModalService {
  bsModalRef: BsModalRef;
  relatedGcl: Subject<string> = new Subject<string>();

  constructor(private modalServiceShow: BsModalService) { }

  openModalRelatedGcl(title: string = 'Modal', relativeTo?: ActivatedRoute, item?: any, modal?: ModalService) {
    const config: ModalOptions = { class: 'modal-sm' };
    item = item ? item : {};
    const initialState = {
      relativeTo: relativeTo,
      item: item,
      title: title,
      modal: modal
    };
    this.bsModalRef = this.modalServiceShow.show(RelatedGclComponent, { initialState, class: 'modal-lg', backdrop: true, ignoreBackdropClick: true, keyboard: false });
    this.bsModalRef.content.onSelectRelateGcl.subscribe(res => {
      console.log(res);
      this.relatedGcl.next(res);
    });
  }

}
