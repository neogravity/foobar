import { RouterModule, Routes } from '@angular/router';
import { NgModule, Component } from '@angular/core';
import { ClaimsComponent } from './claims.component';
import { ClaimListComponent } from './claim-list/claim-list.component';
import { ClaimInfoComponent } from './claim-info/claim-info.component';
import { CreateClaimComponent } from './create-claim/create-claim.component';
import { ClaimTypeConcurrentProcessResolver } from '../../../_resolvers/disease-resolvers';
import { DoctypeResolver } from '../../../_resolvers/doctype-resolvers';

const routes: Routes = [{
  path: '',
  component: ClaimsComponent,
  children: [
    {
      path: 'claim-list',
      component: ClaimListComponent
    },
    {
      path: 'claim-info/:claimsInvoiceId',
      component: ClaimInfoComponent,
      resolve: {
        ClaimTypeConcurrentProcessResolver: ClaimTypeConcurrentProcessResolver,
        DoctypeResolver: DoctypeResolver
      },
    },
    {
      path: 'create-claim/:policyId',
      component: ClaimInfoComponent,
      resolve: {
        ClaimTypeConcurrentProcessResolver: ClaimTypeConcurrentProcessResolver,
        DoctypeResolver: DoctypeResolver
      },
    },
    {
      path: 'create-claim',
      component: CreateClaimComponent,
      resolve: {
        ClaimTypeConcurrentProcessResolver: ClaimTypeConcurrentProcessResolver,
        DoctypeResolver: DoctypeResolver
      },
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ClaimsRoutingModule {
}
