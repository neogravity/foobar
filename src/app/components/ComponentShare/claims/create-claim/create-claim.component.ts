import { Component, OnInit } from '@angular/core';
import { PolicyService } from '../../../../_services/policy.service';
import { SpinnerService } from '../../../../_services/spinner.service';
import { AlertifyService } from '../../../../_services/alertify.service';
import { finalize } from 'rxjs/operators';
import { PaginatedResult, Pagination } from '../../../../_models/pagination';
import { Policie } from '../../../../_models/Policie';
import { RequestListCriteriaDto } from '../../../../DtoView/BaseDto/RequestListCriteriaDto';
import { PolicyFilterDto } from '../../../../DtoView/create-claim/PolicyFilterDto';
import { findLangForName } from '../../../../_helpers/findLangForName';
import { dateDifference } from '../../../../_helpers/dateDifference';
import * as moment from 'moment';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { checkNullOrEmtry } from '../../../../_helpers/checkNullOrEmtry';
import { NbGlobalLogicalPosition, NbToastrService } from '@nebular/theme';

@Component({
  selector: 'ngx-create-claim',
  templateUrl: './create-claim.component.html',
  styleUrls: ['./create-claim.component.scss']
})
export class CreateClaimComponent implements OnInit {
  currentPage = 1;
  page: number;
  totalItems: number;
  pagination: Pagination;
  policy: Policie[] = [];
  findLangForName: Function = findLangForName;
  dateDifference: Function = dateDifference;
  moment: Function = moment;
  createFormSearch: FormGroup;

  constructor(private toastrService: NbToastrService, private policyService: PolicyService, private fb: FormBuilder, private spinner: SpinnerService, private alertify: AlertifyService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.createFormSearch = this.fb.group({
      insuredPersonName: [null],
      insuredPersonSurname: [null],
      policyNo: [null],
      memberNo: [null],
      passportNo: [null],
    });
  }

  btnSearchPolicy(){
    
    const { insuredPersonName, insuredPersonSurname, memberNo, passportNo, policyNo } = this.createFormSearch.value;

    if (checkNullOrEmtry(insuredPersonName)
    && checkNullOrEmtry(insuredPersonSurname)
    && checkNullOrEmtry(memberNo)
    && checkNullOrEmtry(passportNo)
    && checkNullOrEmtry(policyNo)){
      this.toastrService.warning("กรุณากรอกข้อมูลให้ครบ", "Warning", { status: 'primary', position: NbGlobalLogicalPosition.BOTTOM_END })
      return true;
    }

    this.policySearch(this.createFormSearch.value, 1);

  }

  policySearch({...params}, page: number){
    const { insuredPersonName, insuredPersonSurname, memberNo, passportNo, policyNo } = params;

    const requestListCriteriaDto: RequestListCriteriaDto<PolicyFilterDto> = new RequestListCriteriaDto<PolicyFilterDto>();
    requestListCriteriaDto.filter.policyNo = policyNo;
    requestListCriteriaDto.filter.firstname = insuredPersonName;
    requestListCriteriaDto.filter.lastname = insuredPersonSurname;
    requestListCriteriaDto.filter.memberNo = memberNo;
    requestListCriteriaDto.filter.policyCard = passportNo;
    
    this.spinner.spinnerShow();
    this.policyService.policySearch(page, requestListCriteriaDto)
    .pipe(
      finalize(() => this.spinner.spinnerHide())
    )
    .subscribe((res: PaginatedResult<Policie[]>) => {
      this.policy = res.result;
      this.pagination = res.pagination;
      this.totalItems = res.pagination.totalItems;
    }, error => {
      this.alertify.error(error);
    });
  }

  pageChanged(event: any): void {
    this.page = event.page;
    this.policySearch(this.createFormSearch.value, this.page);
  }

  resetForm(){
    this.createFormSearch.reset();
  }

  reserveClaim(item: Policie){
    this.router.navigate([item.id], {relativeTo: this.route});
  }

}
