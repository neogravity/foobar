import { Component } from '@angular/core';
import { SpinnerService } from '../_services/spinner.service';

@Component({
  selector: 'ngx-component',
  styleUrls: ['component.component.scss'],
  templateUrl: './component.component.html',
})
export class ComponentComponent {

  constructor(private spinner: SpinnerService){
    this.spinner.spinnerHide();
  }
}
