import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NbMenuModule, NbCardModule } from '@nebular/theme';
import { ThemeModule } from '../../../@theme/theme.module';
import { DashboardCreditRoutingModule } from './dashboard-credit-routing.module';
import { DashboardCreditComponent } from './dashboard-credit.component';

@NgModule({
  imports: [
    CommonModule,
    ThemeModule,
    NbMenuModule,
    NbCardModule,
    DashboardCreditRoutingModule
  ],
  declarations: [
    DashboardCreditComponent,
  ]
})
export class DashboardCreditModule { }
