import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { DashboardCreditComponent } from './dashboard-credit.component';

const routes: Routes = [{
  path: '',
  component: DashboardCreditComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DashboardCreditRoutingModule {
}
