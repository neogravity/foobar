import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { CreditClaimComponent } from './creditclaim.component';

const routes: Routes = [{
  path: '',
  component: CreditClaimComponent,
  children: [
    {
      path: 'dashboard',
      loadChildren: () => import('./dashboard-credit/dashboard-credit.module').then(m => m.DashboardCreditModule)
    },
    { path: '**', redirectTo: 'dashboard' }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CreditClaimRoutingModule {
}
