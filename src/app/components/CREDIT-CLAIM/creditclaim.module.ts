import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreditClaimComponent } from './creditclaim.component';
import { ThemeModule } from '../../@theme/theme.module';
import { NbMenuModule, NbCardModule } from '@nebular/theme';
import { CreditClaimRoutingModule } from './creditclaim-routing.module';

@NgModule({
  imports: [
    CommonModule,
    CreditClaimRoutingModule,
    ThemeModule,
    NbMenuModule,
    NbCardModule
  ],
  declarations: [
    CreditClaimComponent
  ]
})
export class CreditClaimModule { }
