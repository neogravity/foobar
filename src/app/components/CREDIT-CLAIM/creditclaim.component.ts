import { Component, OnInit } from '@angular/core';
import { MENU_ITEMS } from './creditclaim-menu';
import { HeaderService } from '../../_services/header.service';

@Component({
  selector: 'ngx-faxclaim',
  templateUrl: './creditclaim.component.html',
  styleUrls: ['./creditclaim.component.scss']
})
export class CreditClaimComponent implements OnInit {
  menu = MENU_ITEMS;

  constructor(public headerService: HeaderService) 
  {
    this.headerService.title = 'CREDIT CLAIM';
  }

  ngOnInit() {
  }

}
