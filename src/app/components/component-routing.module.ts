import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { ComponentComponent } from './component.component';
import { AuthGuard } from '../_guards/auth.guard';
import { HomeComponent } from './Othor/home/home.component';

const routes: Routes = [{
  path: '',
  runGuardsAndResolvers: 'always',
  canActivate: [AuthGuard],
  component: ComponentComponent,
  children: [
    {
      path: 'fax',
      loadChildren: () => import('./FAX-CLAIM/faxclaim.module').then(m => m.FaxclcimModule),
    },
    {
      path: 'credit',
      loadChildren: () => import('./CREDIT-CLAIM/creditclaim.module').then(m => m.CreditClaimModule),
    },
    {
      path: 'home',
      component: HomeComponent
    },
    { path: '**', redirectTo: 'home' }
  ]
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ComponentRoutingModule {
}
