import { Component, OnInit } from '@angular/core';
import { MENU_ITEMS } from './faxclaim-menu';
import { HeaderService } from '../../_services/header.service';

@Component({
  selector: 'ngx-faxclaim',
  templateUrl: './faxclaim.component.html',
  styleUrls: ['./faxclaim.component.scss']
})
export class FaxclaimComponent implements OnInit {
  menu = MENU_ITEMS;

  constructor(public headerService: HeaderService) 
  {
    this.headerService.title = 'FAX CLAIM';
  }

  ngOnInit() {
  }

}
