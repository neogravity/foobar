import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FaxclaimComponent } from './faxclaim.component';
import { ThemeModule } from '../../@theme/theme.module';
import { NbMenuModule, NbCardModule, NbTreeGridModule, NbAccordionModule } from '@nebular/theme';
import { FaxclaimRoutingModule } from './faxclaim-routing.module';
import { DashboardConcurrentPopupComponent } from './modals/dashboard/dashboard-concurrent-popup/dashboard-concurrent-popup.component';
import { ModalService } from './modal.service';
import { ModalModule, BsDatepickerModule } from 'ngx-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DashboardConcurrentAddPopupComponent } from './modals/dashboard/dashboard-concurrent-add-popup/dashboard-concurrent-add-popup.component';
import { DataService } from './data.service';
import { FaxclaimService } from '../../_services/faxclaim.service';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { OtherService } from '../../_services/other.service';
import { DirectiveModule } from '../../shared/directives.module';
import { PolicyService } from '../../_services/policy.service';
import { ClaimService } from '../../_services/claim.service';
import { PortalService } from '../../_services/portal.service';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    FaxclaimRoutingModule,
    ThemeModule,
    NbMenuModule,
    NbCardModule,
    ModalModule.forRoot(),
    NbTreeGridModule,
    NbAccordionModule,
    BsDatepickerModule.forRoot(),
    Ng2SearchPipeModule,
    DirectiveModule
  ],
  declarations: [
    FaxclaimComponent,
    DashboardConcurrentPopupComponent,
    DashboardConcurrentAddPopupComponent
  ],
  entryComponents: [
    DashboardConcurrentPopupComponent,
    DashboardConcurrentAddPopupComponent
  ],
  providers: [
    ModalService,
    DataService,
    FaxclaimService,
    OtherService,
    PolicyService,
    ClaimService,
    PortalService
  ],
  exports: [
    DashboardConcurrentPopupComponent,
    DashboardConcurrentAddPopupComponent
  ]
})
export class FaxclcimModule { }
