import { Injectable } from '@angular/core';
import { DashboardListConcurrentViewDto } from '../../DtoView/dashboard/dashboard-list-concurrent-view-dto';
import { formatDate } from '../../_helpers/formatDate';
import { ConcurrentDashboardDto } from '../../DtoView/dashboard/concurrent-dashboard-dto';
import { DischargeDashboardDto } from '../../DtoView/dashboard/discharge-dashboard-dto';
import { DashBoardPreAuthorizedDto } from '../../DtoView/dashboard/dashboard-preauthorized-dto';
import { TodoListDashboardDto } from '../../DtoView/dashboard/todo-list-dashboard-dto';
import { ExpensesDataDto } from '../../DtoView/claim-info/expenses-data-dto';
import { FaxclaimService } from '../../_services/faxclaim.service';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  dashboardListConcurrentViewDto: DashboardListConcurrentViewDto[] = [];
  concurrentDashboardDto: ConcurrentDashboardDto[] = [];
  dischargeDashboardDto: DischargeDashboardDto[] = [];
  dashBoardPreAuthorizedDto: DashBoardPreAuthorizedDto[] = [];
  todoListDashboardDto: TodoListDashboardDto[] = [];

  expensessData1: ExpensesDataDto = <ExpensesDataDto>{};
  expensessData2: ExpensesDataDto = <ExpensesDataDto>{};

  constructor(public faxclaimService: FaxclaimService) {

    // this.expensessData1.ia = {
    //   id: 1,
    //   itemName: 'Personal accident (PA) => IA',
    //   description: 'ค่ารักษาพยาบาล / อุบัติเหตุแต่ละครั้ง 50,000 บาท',
    //   expense: 100,
    //   discount: 10,
    //   total: 90,
    //   coPayment: 0,
    //   cover: 90,
    //   uncover: 0,
    //   deductible: 0,
    //   co: [
    //     {
    //       id: 1,
    //       itemName: 'Personal accident (PA) => CO',
    //       description: 'ค่ารักษาพยาบาล / อุบัติเหตุแต่ละครั้ง 50,000 บาท',
    //       expense: 100,
    //       discount: 10,
    //       total: 90,
    //       coPayment: 0,
    //       cover: 90,
    //       uncover: 0,
    //       deductible: 0,
    //       be: [
    //         {
    //           id: 1,
    //           itemName: 'Personal accident (PA) => BE',
    //           description: 'ค่ารักษาพยาบาล / อุบัติเหตุแต่ละครั้ง 50,000 บาท',
    //           expense: 100,
    //           discount: 10,
    //           total: 90,
    //           coPayment: 0,
    //           cover: 90,
    //           uncover: 0,
    //           deductible: 0,
    //           ex: [
    //             {
    //               id: 1,
    //               itemName: 'Personal accident (PA) => EX 1',
    //               expensesUnit: 100,
    //               unit: 1,
    //               discountMod: 0,
    //               expensesTotal: 0,
    //               discount: 0,
    //               total: 100
    //             },
    //             {
    //               id: 2,
    //               itemName: 'Personal accident (PA) => EX 2',
    //               expensesUnit: 100,
    //               unit: 1,
    //               discountMod: 0,
    //               expensesTotal: 0,
    //               discount: 0,
    //               total: 100
    //             }
    //           ]
    //         },
    //         {
    //           id: 2,
    //           itemName: 'Personal accident (PA) => BE',
    //           description: 'ค่ารักษาพยาบาล / อุบัติเหตุแต่ละครั้ง 40,000 บาท',
    //           expense: 100,
    //           discount: 10,
    //           total: 90,
    //           coPayment: 0,
    //           cover: 90,
    //           uncover: 0,
    //           deductible: 0,
    //           ex: [
    //             {
    //               id: 1,
    //               itemName: 'Personal accident (PA) => EX 1',
    //               expensesUnit: 100,
    //               unit: 1,
    //               discountMod: 0,
    //               expensesTotal: 0,
    //               discount: 0,
    //               total: 100
    //             },
    //             {
    //               id: 2,
    //               itemName: 'Personal accident (PA) => EX 2',
    //               expensesUnit: 100,
    //               unit: 1,
    //               discountMod: 0,
    //               expensesTotal: 0,
    //               discount: 0,
    //               total: 100
    //             }
    //           ]
    //         }
    //       ]
    //     },
    //     {
    //       id: 2,
    //       itemName: 'Personal accident (PA) => CO',
    //       description: 'ค่ารักษาพยาบาล / อุบัติเหตุแต่ละครั้ง 50,000 บาท',
    //       expense: 100,
    //       discount: 10,
    //       total: 90,
    //       coPayment: 0,
    //       cover: 90,
    //       uncover: 0,
    //       deductible: 0,
    //       be: [
    //         {
    //           id: 1,
    //           itemName: 'Personal accident (PA) => BE',
    //           description: 'ค่ารักษาพยาบาล / อุบัติเหตุแต่ละครั้ง 50,000 บาท',
    //           expense: 100,
    //           discount: 10,
    //           total: 90,
    //           coPayment: 0,
    //           cover: 90,
    //           uncover: 0,
    //           deductible: 0,
    //           ex: [
    //             {
    //               id: 1,
    //               itemName: 'Personal accident (PA) => EX 2',
    //               expensesUnit: 100,
    //               unit: 1,
    //               discountMod: 0,
    //               expensesTotal: 0,
    //               discount: 0,
    //               total: 100
    //             }
    //           ]
    //         }
    //       ]
    //     }
    //   ]
    // };
  }

}
