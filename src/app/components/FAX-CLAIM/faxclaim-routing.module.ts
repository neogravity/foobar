import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { FaxclaimComponent } from './faxclaim.component';

const routes: Routes = [{
  path: '',
  component: FaxclaimComponent,
  children: [
    {
      path: 'claims',
      loadChildren: () => import('../ComponentShare/claims/claims.module').then(m => m.ClaimsModule)
    },
    {
      path: 'dashboard',
      loadChildren: () => import('./dashboard-fax/dashboard-fax.module').then(m => m.DashboardFaxModule)
    },
    { path: '**', redirectTo: 'dashboard' }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FaxclaimRoutingModule {
}
