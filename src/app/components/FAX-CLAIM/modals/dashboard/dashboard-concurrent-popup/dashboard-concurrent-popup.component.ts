import { Component, OnInit, ViewChild } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';
import { ActivatedRoute, Router } from '@angular/router';
import { ModalService } from '../../../modal.service';
import { DataService } from '../../../data.service';
import { FaxclaimService } from '../../../../../_services/faxclaim.service';
import { NbToastrService, NbGlobalLogicalPosition } from '@nebular/theme';
import { DashboardListConcurrentViewDto } from '../../../../../DtoView/dashboard/dashboard-list-concurrent-view-dto';
import { SpinnerService } from '../../../../../_services/spinner.service';
import { finalize } from 'rxjs/operators';
import { DomSanitizer } from '@angular/platform-browser';
import * as moment from 'moment';

@Component({
  selector: 'ngx-dashboard-concurrent-popup',
  templateUrl: './dashboard-concurrent-popup.component.html',
  styleUrls: ['./dashboard-concurrent-popup.component.scss']
})
export class DashboardConcurrentPopupComponent implements OnInit {
  @ViewChild('accordion', { static: true }) accordion;

  title: string;
  relativeTo: ActivatedRoute;
  item: any;
  concurrentSearch: string;
  modal: ModalService;
  
  fileUrl;

  constructor(private sanitizer: DomSanitizer, private spinner: SpinnerService, private toastrService: NbToastrService, public FaxclaimService: FaxclaimService, public data: DataService, public bsModalRef: BsModalRef, private route: ActivatedRoute, private router: Router) {
    this.data.dashboardListConcurrentViewDto = [];
  }

  ngOnInit() {
    this.spinner.spinnerShow();
    this.FaxclaimService.getConcurrentHistory(this.item.dashboardList.claimsInvoiceId)
      .pipe(
        finalize(() => this.spinner.spinnerHide())
      )
      .subscribe((res: DashboardListConcurrentViewDto[]) => {
        this.data.dashboardListConcurrentViewDto = res;
      }, error => {
        this.toastrService.danger(error, "Error", { status: 'primary', position: NbGlobalLogicalPosition.BOTTOM_END });
      });
  }

  toggle() {
    this.accordion.toggle();
  }

  btnClose() {
    this.bsModalRef.hide();
  }

  btnAdd() {
    this.bsModalRef.hide();
    this.modal.openModalConcurrentAddItem("Concurrent Add Item - " + this.item.dashboardList.gcl, this.route, { dashboardList: this.item.dashboardList }, this.modal);
  }

  btnSearchConcurrent(concurrentSearch: string) {
    this.concurrentSearch = concurrentSearch;
  }

  btnPrintPdfConcurrentHistoryPdfFile() {
    this.spinner.spinnerShow();
    this.FaxclaimService.getConcurrentHistoryPdfFile(this.item.dashboardList.claimsInvoiceId)
    .pipe(
      finalize(() => this.spinner.spinnerHide())
    ).subscribe((_res) => {
      const blob = new Blob([_res], { type: _res.type });
      var a = document.createElement("a");
          a.href = URL.createObjectURL(blob);
          a.download = 'Concurrent_' + moment().format('YYYY_MM_DD') + '_' + this.item.dashboardList.gcl + '.pdf';
          a.click();
    }); 
  }
}
