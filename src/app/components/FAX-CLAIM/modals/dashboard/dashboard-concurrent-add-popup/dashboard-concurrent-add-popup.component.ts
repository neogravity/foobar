import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BsModalRef } from 'ngx-bootstrap';
import { DataService } from '../../../data.service';
import { formatDate } from '../../../../../_helpers/formatDate';
import { DashboardListConcurrentViewDto } from '../../../../../DtoView/dashboard/dashboard-list-concurrent-view-dto';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../../../../_services/auth.service';
import { BsDatepickerConfig } from 'ngx-bootstrap';
import { FaxclaimService } from '../../../../../_services/faxclaim.service';
import { NbToastrService, NbGlobalLogicalPosition } from '@nebular/theme';
import { SpinnerService } from '../../../../../_services/spinner.service';
import { finalize } from 'rxjs/operators';
import { ModalService } from '../../../modal.service';
import { ConcurrentDashboardDto } from '../../../../../DtoView/dashboard/concurrent-dashboard-dto';

@Component({
  selector: 'ngx-dashboard-concurrent-add-popup',
  templateUrl: './dashboard-concurrent-add-popup.component.html',
  styleUrls: ['./dashboard-concurrent-add-popup.component.scss']
})
export class DashboardConcurrentAddPopupComponent implements OnInit {

  title: string;
  relativeTo: ActivatedRoute;
  item: any;
  modal: any;
  concurrentAddForm: FormGroup;

  bsConfig: Partial<BsDatepickerConfig>;
  colorTheme = 'theme-dark-blue';

  dashboardListConcurrentViewDto: DashboardListConcurrentViewDto = <DashboardListConcurrentViewDto>{};

  constructor(private spinner: SpinnerService, private toastrService: NbToastrService, public faxclaimService: FaxclaimService, private authService: AuthService, private fb: FormBuilder, private data: DataService, public bsModalRef: BsModalRef, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.concurrentAddForm = this.fb.group({
      dateLog: [null, Validators.required],
      disease: [''],
      operationDate: [null, Validators.required],
      operativeProcedure: [''],
      problemPrognosis: [''],
      investigation: [''],
      orderForOneday: [''],
      orderForContinue: [''],
      planDC: [''],
      expenses: [0],
    });

    this.bsConfig = Object.assign({}, {
      containerClass: this.colorTheme,
      dateInputFormat: 'DD-MM-YYYY'
    });
  }

  btnSave() {
    this.dashboardListConcurrentViewDto = this.concurrentAddForm.value;
    this.dashboardListConcurrentViewDto.claimsInvoiceId = this.item.dashboardList.claimsInvoiceId;
    this.dashboardListConcurrentViewDto.expenses = (this.concurrentAddForm.value.expenses < 0 || this.concurrentAddForm.value.expenses == null || this.concurrentAddForm.value.expenses == '') ? 0 : this.concurrentAddForm.value.expenses;
    this.spinner.spinnerShow();
    this.faxclaimService.postConcurrentHistory(this.dashboardListConcurrentViewDto)
    .pipe(
      finalize(() => this.spinner.spinnerHide())
    ).subscribe(async (res: DashboardListConcurrentViewDto[]) => {
      this.data.dashboardListConcurrentViewDto = res;


      //load update dishborad
      this.spinner.spinnerShow();

      await this.faxclaimService.getConcurrentForDashBoard().then((data: ConcurrentDashboardDto[]) => {
        this.data.concurrentDashboardDto = data;
      }, error => {
        this.toastrService.show(null, error, {status: 'danger', position: NbGlobalLogicalPosition.BOTTOM_END});
      });

      this.spinner.spinnerHide();
      //end load update dishborad

      this.saveToClose();
    }, error => {
      this.toastrService.danger(error, "Error", {status: 'primary', position: NbGlobalLogicalPosition.BOTTOM_END});
    });
  }

  btnClose() {
    this.saveToClose();
  }

  saveToClose() {
    this.bsModalRef.hide();
    if (this.modal != null){
      this.modal.openModalConcurrent("Concurrent - " + this.item.dashboardList.gcl, this.route, { dashboardList: this.item.dashboardList }, this.modal);
    }
  }

}
