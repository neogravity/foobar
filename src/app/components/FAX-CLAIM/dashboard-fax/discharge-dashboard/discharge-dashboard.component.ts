import { Component, OnInit } from '@angular/core';
import { DischargeDashboardDto } from '../../../../DtoView/dashboard/discharge-dashboard-dto';
import { ActivatedRoute, Router } from '@angular/router';
import { ModalService } from '../../modal.service';
import { NbToastrService, NbGlobalLogicalPosition } from '@nebular/theme';
import { FaxclaimService } from '../../../../_services/faxclaim.service';
import { SpinnerService } from '../../../../_services/spinner.service';
import { DataService } from '../../data.service';

@Component({
  selector: 'ngx-discharge-dashboard',
  templateUrl: './discharge-dashboard.component.html',
  styleUrls: ['./discharge-dashboard.component.scss']
})
export class DischargeDashboardComponent implements OnInit {
  dischargeSearch: string;
  

  constructor(public data: DataService, private spinner: SpinnerService, public faxclaimService: FaxclaimService, private toastrService: NbToastrService, private modal: ModalService, private router: Router, private route: ActivatedRoute) { }

  async ngOnInit() {
    this.spinner.spinnerShow();

    await this.faxclaimService.getDischargeForDashBoard().then((data: DischargeDashboardDto[]) => {
      this.data.dischargeDashboardDto = data;
    }, error => {
      this.toastrService.show(null, error, {status: 'danger', position: NbGlobalLogicalPosition.BOTTOM_END});
    });

    this.spinner.spinnerHide();
  }

  viewInfoAction(dashboardList: DischargeDashboardDto){
    this.router.navigate(['/component/fax/claims/claim-info/' + dashboardList.claimsInvoiceId]);
  }

  btnSearchDischarge(dischargeSearch: string) {
    this.dischargeSearch = dischargeSearch;
  }

}
