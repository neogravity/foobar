import { Component, OnInit } from '@angular/core';
import { SpinnerService } from '../../../../_services/spinner.service';
import { FaxclaimService } from '../../../../_services/faxclaim.service';
import { NbToastrService, NbGlobalLogicalPosition } from '@nebular/theme';
import { ModalService } from '../../modal.service';
import { Router, ActivatedRoute } from '@angular/router';
import { TodoListDashboardDto } from '../../../../DtoView/dashboard/todo-list-dashboard-dto';
import { DataService } from '../../data.service';

@Component({
  selector: 'ngx-todo-list-dashboard',
  templateUrl: './todo-list-dashboard.component.html',
  styleUrls: ['./todo-list-dashboard.component.scss']
})
export class TodoListDashboardComponent implements OnInit {
  todoListSearch: string;
  

  constructor(public data: DataService, private spinner: SpinnerService, public faxclaimService: FaxclaimService, private toastrService: NbToastrService, private modal: ModalService, private router: Router, private route: ActivatedRoute) { }

  async ngOnInit() {
    this.spinner.spinnerShow();

    await this.faxclaimService.getToDoListForDashBoard().then((data: TodoListDashboardDto[]) => {
      this.data.todoListDashboardDto = data;
    }, error => {
      this.toastrService.show(null, error, { status: 'danger', position: NbGlobalLogicalPosition.BOTTOM_END });
    });

    this.spinner.spinnerHide();
  }

  btnSearchTodoList(todoListSearch: string) {
    this.todoListSearch = todoListSearch;
  }

}
