import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NbMenuModule, NbCardModule, NbTooltipModule, NbBadgeModule } from '@nebular/theme';
import { ThemeModule } from '../../../@theme/theme.module';
import { DashboardFaxRoutingModule } from './dashboard-fax-routing.module';
import { DashboardFaxComponent } from './dashboard-fax.component';
import { DashboardFaxCardComponent } from './dashboard-fax-card/dashboard-fax-card.component';
import { SharedModule } from '../../../shared/shared.module';
import { PreauthorizedDashBoardComponent } from './preauthorized-dashboard/preauthorized-dashboard.component';
import { DischargeDashboardComponent } from './discharge-dashboard/discharge-dashboard.component';
import { TodoListDashboardComponent } from './todo-list-dashboard/todo-list-dashboard.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    DashboardFaxRoutingModule,
  ],
  declarations: [
    DashboardFaxComponent,
    PreauthorizedDashBoardComponent,
    DischargeDashboardComponent,
    TodoListDashboardComponent
  ]
})
export class DashboardFaxModule { }
