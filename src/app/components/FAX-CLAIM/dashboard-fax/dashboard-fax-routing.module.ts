import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { DashboardFaxComponent } from './dashboard-fax.component';

const routes: Routes = [{
  path: '',
  component: DashboardFaxComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DashboardFaxRoutingModule {
}
