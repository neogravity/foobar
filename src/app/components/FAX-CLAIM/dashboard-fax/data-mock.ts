export class DataMock {
    dashboardViewDto = [
        {
            title: 'Pre-Authorized',
            dashboardLists: [
                {
                    gcl: 'GCL-MTI0519-00001',
                    claimType: 'OPD',
                    chanel: 'Hospital',
                    action: [
                        {
                            actionItemName: 'view-info',
                            event: null,
                            icon: 'far fa-edit'
                        }
                    ]
                },
                {
                    gcl: 'GCL-MTI0519-00002',
                    claimType: 'IPD',
                    chanel: 'Backend',
                    action: [
                        {
                            actionItemName: 'view-info',
                            event: null,
                            icon: 'far fa-edit'
                        }
                    ]
                },
                {
                    gcl: 'GCL-MTI0519-00003',
                    claimType: 'IPD',
                    chanel: 'Hospital',
                    action: [
                        {
                            actionItemName: 'view-info',
                            event: null,
                            icon: 'far fa-edit'
                        }
                    ]
                }
            ]
        },
        {
            title: 'Concurrent',
            dashboardLists: [
                {
                    gcl: 'GCL-MTI0519-00001',
                    claimType: 'IPD',
                    chanel: 'Hospital',
                    action: [
                        {
                            actionItemName: 'view-info',
                            event: null,
                            icon: 'far fa-edit'
                        },
                        {
                            actionItemName: 'concurrent',
                            event: null,
                            icon: 'fas fa-phone'
                        },
                    ]
                },
                {
                    gcl: 'GCL-MTI0519-00002',
                    claimType: 'OPD',
                    chanel: 'Backend',
                    action: [
                        {
                            actionItemName: 'view-info',
                            event: null,
                            icon: 'far fa-edit'
                        },
                        {
                            actionItemName: 'concurrent',
                            event: null,
                            icon: 'fas fa-phone'
                        },
                    ]
                },
                {
                    gcl: 'GCL-MTI0519-00003',
                    claimType: 'IPD',
                    chanel: 'Hospital',
                    action: [
                        {
                            actionItemName: 'view-info',
                            event: null,
                            icon: 'far fa-edit'
                        },
                        {
                            actionItemName: 'concurrent',
                            event: null,
                            icon: 'fas fa-phone'
                        },
                    ]
                }
            ]
        },
        {
            title: 'Discharge',
            dashboardLists: [
                {
                    gcl: 'GCL-MTI0519-00001',
                    claimType: 'OPD',
                    chanel: 'Hospital',
                    action: [
                        {
                            actionItemName: 'view-info',
                            event: null,
                            icon: 'far fa-edit'
                        }
                    ]
                },
                {
                    gcl: 'GCL-MTI0519-00002',
                    claimType: 'IPD',
                    chanel: 'Backend',
                    action: [
                        {
                            actionItemName: 'view-info',
                            event: null,
                            icon: 'far fa-edit'
                        }
                    ]
                },
                {
                    gcl: 'GCL-MTI0519-00003',
                    claimType: 'OPD',
                    chanel: 'Hospital',
                    action: [
                        {
                            actionItemName: 'view-info',
                            event: null,
                            icon: 'far fa-edit'
                        }
                    ]
                }
            ]
        },
        {
            title: 'Todo-List',
            dashboardLists: [
                {
                    gcl: 'GCL-MTI0519-00001',
                    claimType: 'OPD',
                    chanel: 'Hospital',
                    action: [
                        {
                            actionItemName: 'view-info',
                            event: null,
                            icon: 'far fa-edit'
                        }
                    ]
                },
                {
                    gcl: 'GCL-MTI0519-00002',
                    claimType: 'IPD',
                    chanel: 'Backend',
                    action: [
                        {
                            actionItemName: 'view-info',
                            event: null,
                            icon: 'far fa-edit'
                        }
                    ]
                },
                {
                    gcl: 'GCL-MTI0519-00003',
                    claimType: 'IPD',
                    chanel: 'Hospital',
                    action: [
                        {
                            actionItemName: 'view-info',
                            event: null,
                            icon: 'far fa-edit'
                        }
                    ]
                }
            ]
        },
    ];
}
