import { Component, OnInit } from '@angular/core';
import { DashboardViewDto } from '../../../DtoView/dashboard/dashboard-view-dto';
import { DashboardActionViewDto } from '../../../DtoView/dashboard/dashboard-action-view-dto';
import { DashboardListViewDto } from '../../../DtoView/dashboard/dashboard-list-view-dto';
import { ModalService } from '../modal.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FaxclaimService } from '../../../_services/faxclaim.service';
import { NbToastrService, NbGlobalLogicalPosition } from '@nebular/theme';
import { SpinnerService } from '../../../_services/spinner.service';

@Component({
  selector: 'ngx-dashboard-fax',
  templateUrl: './dashboard-fax.component.html',
  styleUrls: ['./dashboard-fax.component.scss']
})
export class DashboardFaxComponent implements OnInit {
  dashboardViewDto: DashboardViewDto[] = [];

  constructor(private spinner: SpinnerService, public faxclaimService: FaxclaimService, private toastrService: NbToastrService, private modal: ModalService, private router: Router, private route: ActivatedRoute) { }

  async ngOnInit() {
    // this.spinner.spinnerShow();
    // await this.faxclaimService.getPreAuthorizedForDashBoard().then((data: any) => {
    //   this.dashboardViewDto.push(data);
    // }, error => {
    //   this.toastrService.show(null, error, {status: 'danger', position: NbGlobalLogicalPosition.BOTTOM_END});
    // });

    // await this.faxclaimService.getConcurrentForDashBoard().then((data: any) => {
    //   this.dashboardViewDto.push(data);
    // }, error => {
    //   this.toastrService.show(null, error, {status: 'danger', position: NbGlobalLogicalPosition.BOTTOM_END});
    // });

    // await this.faxclaimService.getDischargeForDashBoard().then((data: any) => {
    //   this.dashboardViewDto.push(data);
    // }, error => {
    //   this.toastrService.show(null, error, {status: 'danger', position: NbGlobalLogicalPosition.BOTTOM_END});
    // });

    // await this.faxclaimService.getToDoListForDashBoard().then((data: any) => {
    //   this.dashboardViewDto.push(data);
    // }, error => {
    //   this.toastrService.show(null, error, {status: 'danger', position: NbGlobalLogicalPosition.BOTTOM_END});
    // });

    // await this.dashboardViewDto.map((m: DashboardViewDto) => {
    //   m.dashboardLists.map((m: DashboardListViewDto) => {
    //     m.action.map((m: DashboardActionViewDto) => {
    //       m.event = this.eventActionFunc.bind(this)
    //     });
    //   });
    // });
    // this.spinner.spinnerHide();
  }

  // eventActionFunc(this, dashboardList: DashboardListViewDto, dashboardAction: DashboardActionViewDto) {
  //   if (dashboardAction.actionItemName == 'view-info') {
  //     console.log(dashboardAction);
  //     this.router.navigate(['/component/fax/claims/claim-info/' + dashboardList.gcl]);
  //   }
  //   else if (dashboardAction.actionItemName == 'concurrent') {
  //     this.modal.openModalConcurrent("Concurrent - " + dashboardList.gcl, this.route, {
  //       dashboardList,

  //     }, this.modal);

  //     console.log(dashboardList);
  //   }
  //   else {
  //     console.log("No Action.");
  //   }
  // }

}
