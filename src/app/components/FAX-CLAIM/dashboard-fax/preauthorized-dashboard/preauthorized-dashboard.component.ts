import { Component, OnInit } from '@angular/core';
import { SpinnerService } from '../../../../_services/spinner.service';
import { FaxclaimService } from '../../../../_services/faxclaim.service';
import { NbToastrService, NbGlobalLogicalPosition } from '@nebular/theme';
import { ModalService } from '../../modal.service';
import { Router, ActivatedRoute } from '@angular/router';
import { DashBoardPreAuthorizedDto } from '../../../../DtoView/dashboard/dashboard-preauthorized-dto';
import { DataService } from '../../data.service';

@Component({
  selector: 'ngx-preauthorized-dashboard',
  templateUrl: './preauthorized-dashboard.component.html',
  styleUrls: ['./preauthorized-dashboard.component.scss']
})
export class PreauthorizedDashBoardComponent implements OnInit {
  preauthorizedSearch: string;
  

  constructor(public data: DataService, private spinner: SpinnerService, public faxclaimService: FaxclaimService, private toastrService: NbToastrService, private modal: ModalService, private router: Router, private route: ActivatedRoute) { }

  async ngOnInit() {
    this.spinner.spinnerShow();

    await this.faxclaimService.getPreAuthorizedForDashBoard().then((data: DashBoardPreAuthorizedDto[]) => {
      this.data.dashBoardPreAuthorizedDto = data;
    }, error => {
      this.toastrService.show(null, error, {status: 'danger', position: NbGlobalLogicalPosition.BOTTOM_END});
    });
    
    this.spinner.spinnerHide();
  }

  viewInfoAction(dashboardList: DashBoardPreAuthorizedDto){
    this.router.navigate(['/component/fax/claims/claim-info/' + dashboardList.claimsInvoiceId]);
  }

  btnSearchPreauthorized(preauthorizedSearch: string) {
    this.preauthorizedSearch = preauthorizedSearch;
  }

}
