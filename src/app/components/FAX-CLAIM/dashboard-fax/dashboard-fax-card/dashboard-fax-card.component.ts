import { Component, OnInit, Input } from '@angular/core';
import { DashboardViewDto } from '../../../../DtoView/dashboard/dashboard-view-dto';

@Component({
  selector: 'ngx-dashboard-fax-card',
  templateUrl: './dashboard-fax-card.component.html',
  styleUrls: ['./dashboard-fax-card.component.scss']
})
export class DashboardFaxCardComponent implements OnInit {
  @Input() dashboardViewDto: DashboardViewDto;

  constructor() { }

  ngOnInit() {
  }

}
