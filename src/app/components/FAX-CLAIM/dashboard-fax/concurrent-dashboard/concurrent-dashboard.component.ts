import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ModalService } from '../../modal.service';
import { NbToastrService, NbGlobalLogicalPosition } from '@nebular/theme';
import { FaxclaimService } from '../../../../_services/faxclaim.service';
import { SpinnerService } from '../../../../_services/spinner.service';
import { ConcurrentDashboardDto } from '../../../../DtoView/dashboard/concurrent-dashboard-dto';
import { DataService } from '../../data.service';

@Component({
  selector: 'ngx-concurrent-dashboard',
  templateUrl: './concurrent-dashboard.component.html',
  styleUrls: ['./concurrent-dashboard.component.scss']
})
export class ConcurrentDashboardComponent implements OnInit {
  concurrentSearch: string;

  constructor(public data: DataService, private spinner: SpinnerService, public faxclaimService: FaxclaimService, private toastrService: NbToastrService, private modal: ModalService, private router: Router, private route: ActivatedRoute) { }

  async ngOnInit() {
    this.spinner.spinnerShow();

    await this.faxclaimService.getConcurrentForDashBoard().then((data: ConcurrentDashboardDto[]) => {
      this.data.concurrentDashboardDto = data;
    }, error => {
      this.toastrService.show(null, error, {status: 'danger', position: NbGlobalLogicalPosition.BOTTOM_END});
    });

    this.spinner.spinnerHide();
  }

  viewInfoAction(dashboardList: ConcurrentDashboardDto){
    this.router.navigate(['/component/fax/claims/claim-info/' + dashboardList.claimsInvoiceId]);
  }

  concurrentAction(dashboardList: ConcurrentDashboardDto){
    this.modal.openModalConcurrent("Concurrent - " + dashboardList.gcl, this.route, {
      dashboardList,

    }, this.modal);
  }

  btnSearchConcurrent(concurrentSearch: string) {
    this.concurrentSearch = concurrentSearch;
  }

}
