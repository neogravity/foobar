import { Injectable } from '@angular/core';
import { BsModalService, BsModalRef, ModalOptions } from 'ngx-bootstrap/modal';
import { ActivatedRoute } from '@angular/router';
import { DashboardConcurrentPopupComponent } from './modals/dashboard/dashboard-concurrent-popup/dashboard-concurrent-popup.component';
import { DashboardConcurrentAddPopupComponent } from './modals/dashboard/dashboard-concurrent-add-popup/dashboard-concurrent-add-popup.component';
@Injectable({
  providedIn: 'root'
})
export class ModalService {
  bsModalRef: BsModalRef;
  constructor(private modalServiceShow: BsModalService) { }

  openModalConcurrent(title: string = 'Modal', relativeTo?: ActivatedRoute, item?: any, modal?: ModalService) {
    const config: ModalOptions = { class: 'modal-sm' };
    item = item ? item : {};
    const initialState = {
      relativeTo: relativeTo,
      item: item,
      title: title,
      modal: modal
    };
    this.bsModalRef = this.modalServiceShow.show(DashboardConcurrentPopupComponent, { initialState, class: 'modal-lg', backdrop: true, ignoreBackdropClick: true, keyboard: false });
  }

  openModalConcurrentAddItem(title: string = 'Modal', relativeTo?: ActivatedRoute, item?: any, modal?: ModalService) {
    const config: ModalOptions = { class: 'modal-sm' };
    item = item ? item : {};
    const initialState = {
      relativeTo: relativeTo,
      item: item,
      title: title,
      modal: modal
    };
    this.bsModalRef = this.modalServiceShow.show(DashboardConcurrentAddPopupComponent, { initialState, class: 'modal-sm', backdrop: true, ignoreBackdropClick: true, keyboard: false });
  }

}
