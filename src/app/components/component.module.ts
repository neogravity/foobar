import { NgModule } from '@angular/core';
import { ComponentComponent } from './component.component';
import { ComponentRoutingModule } from './component-routing.module';
import { HomeComponent } from './Othor/home/home.component';
import { CommonModule } from '@angular/common';
import { NbCardModule, NbLayoutModule, NbIconModule } from '@nebular/theme';
import { AlertifyService } from '../_services/alertify.service';

@NgModule({
  imports: [
    CommonModule,
    ComponentRoutingModule,
    NbCardModule,
    NbLayoutModule,
    NbIconModule
  ],
  declarations: [
    ComponentComponent,
    HomeComponent
  ],
  providers: [
    AlertifyService
  ],
})
export class ComponentModule { }
