import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot } from '@angular/router';
import { AuthService } from '../_services/auth.service';
import { AlertifyService } from '../_services/alertify.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private authService: AuthService, private router: Router,
    private alertify: AlertifyService) { }

  canActivate(next: ActivatedRouteSnapshot): boolean {
    if (this.authService.loggedIn()) {
      // let roles: any;
      // if (next.firstChild == null) {
      //   next.children.forEach(_first => {
      //     _first.children.forEach(_second => {
      //       if (typeof _second.data['roles'] !== 'undefined') {
      //         roles = _second.data['roles'] as Array<string>;
      //       }
      //     });
      //   });
      // } else {
      //   roles = next.firstChild.data['roles'] as Array<string>;
      // }

      // if (roles) {
      //   const match = this.authService.roleMatch(roles);
      //   if (match) {
      //     return true;
      //   } else {
      //     this.router.navigate(['/usersetting']);
      //     this.alertify.error('You are not authorized to access this area');
      //   }
      // }
      return true;
    }

    // this.alertify.error('You shall not pass!!!');
    this.router.navigate(['/login']);
    return false;
  }
}
