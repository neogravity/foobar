import { People } from './People';

export interface PeopleAddress {
    id: number;
    version: number;
    isStatus: string;
    sort: number;
    peopleId: number;
    address: string;
    postCode: string;
    subDistrict: string;
    district: string;
    province: string;
    mobile: string;
    email: string;
    createdAt: Date;
    updatedAt: Date;
    createBy: number;
    updateBy: number;
    peoples: People;
}
