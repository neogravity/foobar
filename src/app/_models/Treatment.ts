import { TreatmentLang } from './TreatmentLang';

export interface Treatment {
    id: number;
    isStatus: string;
    sort: number;
    createdAt: Date;
    updatedAt: Date;
    createBy: number;
    updateBy: number;
    treatmentLangs: Array<TreatmentLang>;
}
