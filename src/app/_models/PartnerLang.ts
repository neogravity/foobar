export interface PartnerLang {
    id: number;
    language: string;
    name: string;
}
