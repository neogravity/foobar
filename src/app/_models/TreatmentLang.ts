export interface TreatmentLang {
    id: number;
    language: string;
    name: string;
}
