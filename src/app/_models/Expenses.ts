import { ExpensesLang } from './ExpensesLang';

export interface Expenses {
    id: number;
    isStatus: string;
    sort: number;
    expensesItemCode: string;
    unitId: number;
    createdAt: Date;
    updatedAt: Date;
    createBy: number;
    updateBy: number;
    expensesLangs: Array<ExpensesLang>;
}
