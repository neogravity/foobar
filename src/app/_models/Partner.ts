import { PartnerLang } from './PartnerLang';

export interface Partner {
    id: number;
    isStatus: string;
    sort: number;
    shortName: string;
    longName: string;
    perfixGCL: string;
    remark: string;
    effectiveDate: Date;
    createdAt: Date;
    updatedAt: Date;
    createBy: number;
    updateBy: number;
    partnerLangs: Array<PartnerLang>;
}
