import { GenderLang } from './GenderLang';

export interface Gender {
    id: number;
    isStatus: string;
    sort: number;
    code: string;
    createdAt: Date;
    updatedAt: Date;
    createBy: number;
    updateBy: number;
    genderLangs: GenderLang;
}
