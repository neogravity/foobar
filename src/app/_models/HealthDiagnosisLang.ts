export interface HealthDiagnosisLang {
    id: number;
    language: string;
    name: string;
}