import { Policie } from './Policie';

export interface PolicieCard {
    id: number;
    version: number;
    isStatus: string;
    sort: number;
    partnerId: number;
    cardTypeId: number;
    cardNoTypeId: number;
    cardNo: string;
    itemNo: string;
    issueDate: Date;
    policyId: number;
    remark: string;
    reasonReprint: string;
    flagCardFee: number;
    createdAt: Date;
    updatedAt: Date;
    createBy: number;
    updateBy: number;
    policies: Policie;
}
