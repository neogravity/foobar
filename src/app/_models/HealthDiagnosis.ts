import { HealthDiagnosisLang } from './HealthDiagnosisLang';

export interface HealthDiagnosis {
    id: number;
    isStatus: string;
    sort: number;
    code: string;
    createdAt: Date;
    updatedAt: Date;
    createBy: number;
    updateBy: number;
    healthDiagnosisLangs: Array<HealthDiagnosisLang>;
}