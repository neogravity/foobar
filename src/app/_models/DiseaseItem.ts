import { DiseaseItemLang } from './DiseaseItemLang';

export interface DiseaseItem {
    id?: number;
    isStatus?: string;
    sort?: number;
    diseaseGroupId?: number;
    code: string;
    icdType?: string;
    createBy?: number;
    updateBy?: number;
    diseaseItemLangs?: Array<DiseaseItemLang>;
}
