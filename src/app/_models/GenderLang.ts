import { Gender } from './Gender';

export interface GenderLang {
    id: number;
    language: string;
    name: string;
    genders: Gender;
}
