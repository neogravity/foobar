export interface DiseaseItemLang {
    id: number;
    language: string;
    name: string;
}
