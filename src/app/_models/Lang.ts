export class Lang {
    id: string;
    version: string;
    language: string;
    name: string;
}
