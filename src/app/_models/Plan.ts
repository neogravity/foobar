export interface Plan {
    id: string;
    isStatus: string;
    sort: string;
    code: string;
    createdAt: string;
    updatedAt: string;
    createBy: string;
    updateBy: string;
    productId: string;
    partnerId: string;
    updateByUser: string;
    createByUser: string;
    product: string;
    partner: string;
    planLang: string;
    planTypeId: string;
    planType: string;
    policyExclusion: string;
    planOtherBenefit: string;
}
