import { PeopleLang } from './PeopleLang';
import { PeopleAddress } from './PeopleAddress';
import { Policie } from './Policie';
import { GenderLang } from './GenderLang';

export interface People {
    id: number;
    isStatus: string;
    sort: number;
    version: number;
    partnerId: number;
    identityTypeId: number;
    identityId: string;
    principalId: string;
    bloodId: number;
    genderId: number;
    titleId: number;
    birthDay: Date;
    remark: string;
    nationidPassport: string;
    createdAt: Date;
    updatedAt: Date;
    createBy: number;
    updateBy: number;
    peopleLangs: Array<PeopleLang>;
    peopleAddress: Array<PeopleAddress>;
    policies: Policie;
    genders: GenderLang;
}
