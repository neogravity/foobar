import { Lang } from './Lang';

export class Hospital {
    id: number;
    name: string;
    hospitalsLangs?: Array<Lang>;
}
