import { People } from './People';

export interface PeopleLang {
    id: number;
    version: number;
    language: string;
    name: string;
    middleName: string;
    surname: string;
    peoples: People;
}
