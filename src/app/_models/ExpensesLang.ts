export interface ExpensesLang {
    id: number;
    language: string;
    name: string;
}
