export interface User {
    id: number;
    username: string;
    password: string;
    firstName: string;
	lastName: string;
    email: string;
    phoneNumber: string;
    userRole: number[];
    userTypeId: number[];
    partnerId: number[];
    hospitalId: number[];
	dateOfBirth?: Date;
}
